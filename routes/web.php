<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|testfolks@gmail.com TestFolks@iru
testverona@gmail.com TestVerona@iru
testpaseosnauticos@gmail.com TestNauticos@iru
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|Route::middleware(['auth'])->group(function () {
*/

Route::get('/', function () {
    return view('auth.login');
});


Route::get('/login', 'AuthController@login')->name('login');
Route::get('/logout', 'AuthController@logout');
Route::post('/login', 'AuthController@authenticate');
    Route::middleware(['system'])->group(function () {
        Route::get('/home', 'DashboardController@index');
        Route::post('/report', 'DashboardController@showReport');
        Route::resource('User', 'UserController');
        Route::resource('Client', 'ClientController');
        Route::get('/User/{id}/changePassword', 'UserController@showpassword');
        Route::post('User/{id}/password', 'UserController@password');
        Route::resource('Item', 'ItemController');
        //Route::get('/Outstanding', 'OutstandingController@index');
        //Route::get('/{idCategory}/{valCheck}/get_outstanding', 'OutstandingController@getOutstanding');
        Route::get('/ListOutstanding/{checks}/{idCategory}', 'OutstandingController@storeOutstanding');
        Route::get('/SaveRanking/{idItem}/{newRanking}', 'OutstandingController@storeRanking');
        Route::resource('Multimedia', 'MultimediaController');
        Route::get('/Main', 'MultimediaController@indexMain');
        Route::post('/imageMain/{id}/{name}', 'MultimediaController@storeMain');
        Route::get('/Setting', 'SettingController@index');
        Route::resource('SubCategory', 'SubCategoryController');
        Route::get('/{id}/get_subcategory', 'SubCategoryController@getSubCategoryByCategory');
        Route::get('/getCall/{msg}/', 'DashboardController@getCall');
        Route::get('/getSearches', 'DashboardController@getSearcheOfItems');
        Route::get('/{id}/get_itemByCategory', 'OutstandingController@getItemByCategory');
        Route::get('/{id}/get_outstandingByCategory', 'OutstandingController@getOutstandingByCategory');
        Route::get('/outstanding/{id}', 'ItemController@getOutstandingOfItem');
        
        //Report of searches
        Route::get('/RsearchesByCategory', 'DashboardController@RsearchesByCategory');
        Route::get('/RsearchesBySubCategory', 'DashboardController@RsearchesBySubCategory');
        Route::get('/RsearchesByItem', 'DashboardController@RsearchesByItem');
        Route::get('/RsearchesBySubscription', 'DashboardController@RsearchesBySuscription');

        //Repor of calls
        Route::get('/RcallBySubscription', 'DashboardController@RcallBySuscription');
        Route::get('/RcallByCategory', 'DashboardController@RcallByCategory');
        Route::get('/RcallBySubCategory', 'DashboardController@RcallBySubCategory');
        Route::get('/RcallByItem', 'DashboardController@RcallByItem');

        //Report of searches By Month
        Route::get('/RsearchesOfCategoryByMonth', 'DashboardController@RsearchesOfCategoryByMonth');
        Route::get('/RsearchesOfSubCategoryByMonth', 'DashboardController@RsearchesOfSubCategoryByMonth');
        Route::get('/RsearchesOfItemByMonth', 'DashboardController@RsearchesOfItemByMonth');
        Route::get('/RsearchesOfSubscriptionByMonth', 'DashboardController@RsearchesOfSubscriptionByMonth');

        //Report of searches By Month
        Route::get('/RcallsOfCategoryByMonth', 'DashboardController@RcallsOfCategoryByMonth');
        Route::get('/RcallsOfSubCategoryByMonth', 'DashboardController@RcallsOfSubCategoryByMonth');
        Route::get('/RcallsOfItemByMonth', 'DashboardController@RcallsOfItemByMonth');
        Route::get('/RcallsOfSubscriptionByMonth', 'DashboardController@RcallsOfSubscriptionByMonth');



        // Route::get('/updateRolsdeUsuarios', 'RolsController@index');
        // Route::get('/updateRolsdeUsuarios/{id}', 'RolsController@edit');
        // Route::post('/updateRolsdeUsuarios/{id}', 'RolsController@update');

    });

    Route::middleware(['client'])->group(function () {
        Route::get('/dashboard', 'DashboardItemController@index');
        Route::get('/searche7days/{id}', 'DashboardItemController@SearcheOfItem7days');
        Route::get('/call7days/{id}', 'DashboardItemController@CallOfItem7days');
        Route::get('/yearForMonth/{id}', 'DashboardItemController@getSearcheForMonths');
        Route::get('/yearForMonthCall/{id}', 'DashboardItemController@getCallForMonths');


        
        
        

    });

