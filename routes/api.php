<?php

use Illuminate\Http\Request;
use App\Item;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/{idcategory}/items', 'ItemController@getItemsForCategory');
Route::get('/subcategory/{idsubcategory}/items', 'ItemController@getItemsForSubCategory');
Route::get('/items/{query}', 'ItemController@getQueryItems');
Route::get('/subcategory/{idsubcategory}/newsitems', 'ItemController@getNewsItemsForSubCategory');
Route::get('/categories', 'CategoryController@getCategories');
Route::get('/item/{id}', 'ItemController@getDetailsOfItem');
Route::get('/{idcategory}/subcategories', 'SubCategoryController@getApiSubCategoriesByCategory');
Route::get('/deliverys', 'DeliveryController@getDeliverys');
Route::get('/outstanding/{id}', 'ItemController@getOutstandingOfItem');
 //For Reports
 Route::post('/infocall', 'ReportsController@infoCall');
 Route::post('/infoitem', 'ReportsController@infoItem');