@extends('master')

@section('stylus')
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}"/>

@endsection
@section('content_admin')

<div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="fa flaticon-cogwheel-2"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Categorías
                            </h3>
                        </div>
                    </div>
                </div>
                
            
                <form 
                    class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state" 
                    method="POST" 
                    action="{{ url('SubCategory') }}"
                    enctype="multipart/form-data">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="m-portlet__body">
                        @if (session('typemsg'))
                            @if (session('typemsg') == 'success')
                                <div class="alert alert-success">
                                <strong><p>{{ session('message') }}</p></strong>
                                </div>
                            @endif
                            @if (session('typemsg') == 'error')
                                <div class="alert alert-danger">
                                <strong><p>{{ session('message') }}</p></strong>
                                </div>
                            @endif	
                        @endif	
                        
                       
                        <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label for="exampleSelect1">
                                        <strong> Perfil: </strong>
                                            
                                        </label>
                                        <select name="category" class="form-control m-input " id="category">

                                            @foreach ($categories as $category)
                                                    <option value="{{ $category->id }}" @if ($category->id == 1) selected @endif
                                                        @if (old('category'))
                                                            @if (old('category') == $category->id)
                                                                selected
                                                            @endif
                                                        @endif
                                                        >
                                                        {{$category->name}}
                                                    </option>
                                            @endforeach

                                        </select>
                                </div>	
                                
                                <div class="col-lg-6 @if($errors->has('name')) has-danger @endif">
                                    <label>
                                        <strong> Categoría: </strong> 
                                    </label>
                                    <input type="text" name="name" id="Name" class="form-control m-input @if($errors->has('name')) form-control-danger @endif" placeholder="Nombre" value="{{ old('name') }}">
                                    
                                    @if ($errors->any())
                                        @if($errors->has('name'))
                                        <div class="form-control-feedback">
                                            Por favor ingrese un nombre
                                        </div>
                                        @endif
                                    @endif
                                </div>
                        </div>
                        <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <input type="file" name="icon" id="imgInp">
                                    @if ($errors->any())
                                        @if($errors->has('icon'))
                                        <div class="help-block with-errors">
                                            {{ $errors->first('icon') }}
                                        </div>
                                        @endif
                                    @endif
                                </div>
                                <div class="col-lg-6">
                                    <img id="target" src="#"  alt="el ícono"/>
                                </div>
                        </div>

                       
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" id="Save" class="btn btn-primary">
                                            Guardar
                                        </button>
                                        <a href="{{url('Setting')}}" class="btn btn-secondary">
                                        <span>
                                                Volver
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </form>
                <!--end::Form-->
               
        </div>
@endsection


@section('script')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();            
            reader.onload = function (e) {
                $('#target').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function(){
        readURL(this);
    });
</script>    
@endsection