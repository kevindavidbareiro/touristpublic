<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{asset('css/styleReport.css')}}" rel="stylesheet" type="text/css" />
    <style>
            /*div.breakNow { page-break-inside:avoid; page-break-after:always; }*/
            tr:nth-child(even) {background-color: #cacaca;}
    </style>
    <title>Cantidad de busquedas por Categorías</title>
</head>
<body>
    <center><h2>REPORTE</h2></center>
   
   
    <h3>Cantidad de busquedas de Categorías por mes:  {{$data->format('d/m/Y')}} - {{$data->format('H:i')}}</h3>
    
    @foreach($listCategoryByMonth as $key => $categoryByMonth)
        <h3>{{$key}}</h3>
        <div class="container">
            
            <table class="tableReport">
                <thead class="thReport">
                    <tr>
                        <th>
                            Categoría
                        </th>
                        <th>
                            Cantidad de busquedas
                        </th>
                    </tr>
                </thead>
                <tbody class="tdReport">
                    @foreach($categoryByMonth as $item =>$category)
                        <tr>
                            <td>
                                {{ $item}}
                            </td>
                            <td>
                                {{ $category}}
                            </td>
                        </tr> 
                    @endforeach
                </tbody>
            </table>
        </div>
    @endforeach

</body>
</html>



        