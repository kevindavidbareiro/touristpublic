<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{asset('css/styleReport.css')}}" rel="stylesheet" type="text/css" />
    <style>
            /*div.breakNow { page-break-inside:avoid; page-break-after:always; }*/
            tr:nth-child(even) {background-color: #cacaca;}
    </style>
    <title>Cantidad de llamadas por Mese</title>
</head>
<body>
    <center><h2>REPORTE</h2></center>
   
   
    <h3>Cantidad de llamadas por Mese:  {{$data->format('d/m/Y')}} - {{$data->format('H:i')}}</h3>
    <h3>Total de llamadas {{ $totalCalls }}</h3>
    <div class="container">
        <table class="tableReport">
            <thead class="thReport">
                <tr>
                    <th>
                        Meses
                    </th>
                    <th>
                        Cantidad de llamadas
                    </th>
                </tr>
            </thead>
            <tbody class="tdReport">
               
                <tr>
                    <td>
                        Octubre
                    </td>
                    <td>
                        {{ $countCallByMonths[9] }}
                    </td>
                </tr>
                <tr>
                    <td>
                        Noviembre
                    </td>
                    <td>
                        {{ $countCallByMonths[10] }}
                    </td>
                </tr>  
               
            </tbody>
        </table>
    </div>
</body>
</html>



        