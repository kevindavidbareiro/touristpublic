<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{asset('css/styleReport.css')}}" rel="stylesheet" type="text/css" />
    <style>
            /*div.breakNow { page-break-inside:avoid; page-break-after:always; }*/
            tr:nth-child(even) {background-color: #cacaca;}
    </style>
    <title>Cantidad de llamadas por Subcategorías</title>
</head>
<body>
    <center><h2>REPORTE</h2></center>
   
   
    <h3>Cantidad de llamadas por Subcategorías:  {{$data->format('d/m/Y')}} - {{$data->format('H:i')}}</h3>

    <div class="container">
        <table class="tableReport">
            <thead class="thReport">
                <tr>
                    <th>
                        Subcategoría
                    </th>
                    <th>
                        Categoría
                    </th>
                    <th>
                        Cantidad de llamadas
                    </th>
                </tr>
            </thead>
            <tbody class="tdReport">
                @foreach($listCallSubCategories as $callsubcategory)
                    <tr>
                        <td>
                            {{ $callsubcategory[1]}}
                        </td>
                        <td>
                            {{ $callsubcategory[2] }}
                        </td>
                        <td>
                            {{ $callsubcategory[3] }}
                        </td>
                    </tr> 
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>



        