
@extends('master')

@section('stylus')
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}"/>

@endsection
@section('content_admin')
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
					<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
									Busquedas
								</h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<i class="m-nav__link-icon la flaticon-search-1"></i>
									</li>
									<li class="m-nav__separator">
										Por -
									</li>
									<li class="m-nav__item">
                                        <span class="m-nav__link-text">
                                            Categorías
                                        </span>
                                    </li>
                                    <li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
                                        <span class="m-nav__link-text">
                                            Subcategorías
                                        </span>
									</li>
                                    <li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
                                        <span class="m-nav__link-text">
                                            Items
                                        </span>
									</li>
								</ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-xl-6">
                            <div class="m-content table-scatecory">
                                <div class="m-portlet m-portlet--mobile">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h2 class="m-portlet__head-text">
                                                    <i class="la flaticon-statistics"></i>
                                                    Busquedas por categoría
                                                </h2>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="m-portlet__body">		
                                 <!--end: Search Form -->
                                        <div class="btn-export-pdf">
                                            <a href="{{ url('RsearchesByCategory') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar
                                                </button>
                                            </a>
                                            <a href="{{ url('RsearchesOfCategoryByMonth') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar por Mes
                                                </button>
                                            </a>
                                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                                        </div>
                                           
                                <!--begin: Datatable -->
                                        <div class="table-wrapper-scroll-y my-custom-scrollbar">

                                            <table class="table table-striped table-hover" id="table-categories" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            Categoría
                                                        </th>
                                                        <th>
                                                            Cantidad de busquedas
                                                        </th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($listCategory as $category)

                                                            <tr>
                                                                <td>
                                                                    {{ $category[1]}}
                                                                </td>
                                                                <td>
                                                                    {{ $category[2] }}
                                                                </td>
                                                                
                                                            </tr> 
                                                        
                                                    @endforeach
                                                </tbody>
                                            
                                            </table>
                                            <!--end: Datatable -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>
                    <div class="col-xl-6">
                            <div class="m-content">
                                <div class="m-portlet m-portlet--mobile">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h2 class="m-portlet__head-text">
                                                    <i class="la flaticon-statistics"></i>
                                                    Busquedas por subcategorías
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        <div class="btn-export-pdf">
                                            <a href="{{ url('RsearchesBySubCategory') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar
                                                </button>
                                            </a>
                                            <a href="{{ url('RsearchesOfSubCategoryByMonth') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar por Mes
                                                </button>
                                            </a>
                                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                                        </div>	
                                        <!--end: Search Form -->
                                <!--begin: Datatable m-datatable my-table-->
                                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                            <table class="table table-striped table-hover" id="table-subcategories" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            SubCategoría
                                                        </th>
                                                        <th>
                                                            Categoría
                                                        </th>
                                                        <th>
                                                            Cantidad de busquedas
                                                        </th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($listSubcategory as $subcategory)

                                                            <tr>
                                                                <td>
                                                                    {{ $subcategory[1]}}
                                                                </td>
                                                                <td>
                                                                    {{ $subcategory[2] }}
                                                                </td>
                                                                <td>
                                                                    {{ $subcategory[3] }}
                                                                </td>
                                                                
                                                            </tr> 
                                                        
                                                    @endforeach
                                                </tbody>
                                            
                                            </table>
                                        <!--end: Datatable -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>
                    <div class="col-xl-6">
                            <div class="m-content">
                                <div class="m-portlet m-portlet--mobile">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h2 class="m-portlet__head-text">
                                                    <i class="la flaticon-statistics"></i>
                                                    Busquedas por Item
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        <div class="btn-export-pdf">
                                            <a href="{{ url('RsearchesByItem') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar
                                                </button>
                                            </a>
                                            <a href="{{ url('RsearchesOfItemByMonth') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar por Mes
                                                </button>
                                            </a>
                                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                                        </div>	
                                        
                                        <!--end: Search Form -->
                                <!--begin: Datatable -->
                                       <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                            <table class="table table-striped table-hover" id="table-items" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            Item
                                                        </th>
                                                        <th>
                                                            Categoría
                                                        </th>
                                                        <th>
                                                            Cantidad de busquedas
                                                        </th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($listItems as $item)

                                                        <tr>
                                                            <td>
                                                                {{ $item[1]}}
                                                            </td>
                                                            <td>
                                                                {{ $item[2] }}
                                                            </td>
                                                            <td>
                                                                {{ $item[3] }}
                                                            </td>
                                                        </tr> 
                                                        
                                                    @endforeach
                                                </tbody>
                                            
                                            </table>
                                        </div>
                                        <!--end: Datatable -->
                                    </div>
                                </div>
                            </div>

                    </div>
                    <div class="col-xl-6">
                            <div class="m-content">
                                <div class="m-portlet m-portlet--mobile">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h2 class="m-portlet__head-text">
                                                    <i class="la flaticon-statistics"></i>
                                                    Busquedas por Suscripción
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        <div class="btn-export-pdf">
                                            <a href="{{ url('RsearchesBySubscription') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar
                                                </button>
                                            </a>
                                            <a href="{{ url('RsearchesOfSubscriptionByMonth') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar por Mes
                                                </button>
                                            </a>
                                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                                        </div>	
                                        
                                        <!--end: Search Form -->
                                <!--begin: Datatable -->
                                       <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                            <table class="table table-striped table-hover" id="table-items" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            Suscripción
                                                        </th>
                                                        <th>
                                                            Cantidad de busquedas
                                                        </th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($listSuscription as $key => $suscription)
                                                    
                                                        <tr>
                                                            <td>
                                                                {{ $key}}
                                                            </td>
                                                            <td>
                                                                {{ $suscription}}
                                                            </td>
                                                            
                                                        </tr> 
                                                        
                                                    @endforeach
                                                </tbody>
                                            
                                            </table>
                                        </div>
                                        <!--end: Datatable -->
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>


            <div class="m-grid__item m-grid__item--fluid m-wrapper">
					<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
									Llamadas
								</h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<i class="m-nav__link-icon la flaticon-search-1"></i>
									</li>
									<li class="m-nav__separator">
										Por -
									</li>
									<li class="m-nav__item">
                                        <span class="m-nav__link-text">
                                            Categorías
                                        </span>
                                    </li>
                                    <li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
                                        <span class="m-nav__link-text">
                                            Subcategorías
                                        </span>
									</li>
                                    <li class="m-nav__separator">
										-
									</li>
									<li class="m-nav__item">
                                        <span class="m-nav__link-text">
                                            Items
                                        </span>
									</li>
								</ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4">
                        <div class="m-content">
                            <!--begin:: Widgets/Inbound Bandwidth-->
                            
                                <div class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit " style="min-height: 300px">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h3 class="m-portlet__head-text">	
                                                <i class="la flaticon-support"></i>  
                                                    Llamadas por Por Mes
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="btn-export-pdf2">
                                                <a href="{{ url('getCall/'. 'report' .'/') }}" target=_blank>    
                                                    <button  type="btn" class="btn btn-primary">
                                                        Exportar
                                                    </button>
                                                </a>
                                                <div class="m-separator m-separator--dashed d-xl-none"></div>
                                            </div>		
                                        </div>
                                    <div class="m-portlet__body">
                                        <!--begin::Widget5-->
                                        <div class="m-widget20">
                                            <div class="m-widget20__number m--font-success totalCall">
                                                
                                            </div>
                                            <div class="m-widget20__chart" style="height:160px;">
                                                <canvas id="call_by_month"></canvas>
                                            </div>
                                        </div>
                                        <!--end::Widget 5-->
                                    </div>
                                    
                                </div>
                            <div class="table-wrapper-scroll-y-call my-custom-scrollbar-call">
                                <div class="m-portlet m-portlet--bordered-semi">
                                    <!--end:: Widgets/Inbound Bandwidth-->
                                    <div class="m-portlet__body">
                                            <!--begin::Widget 6-->
                                            <div class="m-widget15">
                                                <div class="m-widget15__items">
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="m-widget15__item">
                                                                <span class="m-widget15__stats callJanuary">
                                                                
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="m-widget15__item">
                                                                <span class="m-widget15__stats callFebruary">
                                                                
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="m-widget15__item">
                                                                <span class="m-widget15__stats callMarch">
                                                                
                                                                </span>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="m-widget15__item">
                                                                <span class="m-widget15__stats callApril">
                                                                
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="m-widget15__item">
                                                                <span class="m-widget15__stats callMay">
                                                                
                                                                </span>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col">
                                                            <div class="m-widget15__item">
                                                                <span class="m-widget15__stats callJune">
                                                                
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="m-widget15__item">
                                                                <span class="m-widget15__stats callJuly">
                                                                
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="m-widget15__item">
                                                                <span class="m-widget15__stats callAugust">
                                                                
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="m-widget15__item">
                                                                <span class="m-widget15__stats callSeptember">
                                                                
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="m-widget15__item">
                                                                <span class="m-widget15__stats callOctober">
                                                                
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="m-widget15__item">
                                                                <span class="m-widget15__stats callNovember">
                                                                
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="m-widget15__item">
                                                                <span class="m-widget15__stats callDecember">
                                                                
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end::Widget 6-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-7">
                            <div class="m-content">
                                <div class="m-portlet m-portlet--mobile">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h2 class="m-portlet__head-text">
                                                    <i class="la flaticon-statistics"></i>
                                                    Llamadas por Categoría
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        <div class="btn-export-pdf">
                                            <a href="{{ url('RcallByCategory') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar
                                                </button>
                                            </a>
                                            <a href="{{ url('RcallsOfCategoryByMonth') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar por Mes
                                                </button>
                                            </a>
                                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                                        </div>			
                                      
                                        <!--end: Search Form -->
                                <!--begin: Datatable -->
                                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                            <table class="table table-striped table-hover" id="table-items" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            Categoría
                                                        </th>
                                                        <th>
                                                            Llamadas
                                                        </th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($listCallCategories as $callcategory)

                                                            <tr>
                                                                <td>
                                                                    {{ $callcategory[1]}}
                                                                </td>
                                                                <td>
                                                                    {{ $callcategory[2] }}
                                                                </td>
                                                                
                                                            </tr> 
                                                        
                                                    @endforeach
                                                </tbody>
                                            
                                            </table>
                                        </div>
                                        <!--end: Datatable -->
                                    </div>
                                </div>
                            </div>

                    </div>
                    <div class="col-xl-6">
                            <div class="m-content">
                                <div class="m-portlet m-portlet--mobile">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h2 class="m-portlet__head-text">
                                                    <i class="la flaticon-statistics"></i>
                                                    Llamadas por subcategorías
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        <div class="btn-export-pdf">
                                            <a href="{{ url('RcallBySubCategory') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar
                                                </button>
                                            </a>
                                            <a href="{{ url('RcallsOfSubCategoryByMonth') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar por Mes
                                                </button>
                                            </a>
                                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                                        </div>	
                                      
                                        <!--end: Search Form -->
                                <!--begin: Datatable -->
                                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                            <table class="table table-striped table-hover" id="table-subcategories" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            SubCategoría
                                                        </th>
                                                        <th>
                                                            Categoría
                                                        </th>
                                                        <th>
                                                            Llamadas
                                                        </th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($listCallSubCategories as $callsubcategory)

                                                        <tr>
                                                            <td>
                                                                {{ $callsubcategory[1]}}
                                                            </td>
                                                            <td>
                                                                {{ $callsubcategory[2] }}
                                                            </td>
                                                            <td>
                                                                {{ $callsubcategory[3] }}
                                                            </td>
                                                            
                                                        </tr> 

                                                    @endforeach
                                                </tbody>
                                            
                                            </table>
                                        </div>
                                        <!--end: Datatable -->
                                    </div>
                                </div>
                            </div>

                    </div>

                    <div class="col-xl-6">
                            <div class="m-content">
                                <div class="m-portlet m-portlet--mobile">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h2 class="m-portlet__head-text">
                                                    <i class="la flaticon-statistics"></i>
                                                    Llamadas por Item
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        <div class="btn-export-pdf">
                                            <a href="{{ url('RcallByItem') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar
                                                </button>
                                            </a>
                                            <a href="{{ url('RcallsOfItemByMonth') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar por Mes
                                                </button>
                                            </a>
                                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                                        </div>		
                                        <!--end: Search Form -->
                                <!--begin: Datatable -->
                                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                            <table class="table table-striped table-hover" id="table-items" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            Item
                                                        </th>
                                                        <th>
                                                            Categoría
                                                        </th>
                                                        <th>
                                                            Llamadas
                                                        </th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($listCallItems as $callitem)

                                                            <tr>
                                                                <td>
                                                                    {{ $callitem[1]}}
                                                                </td>
                                                                <td>
                                                                    {{ $callitem[2] }}
                                                                </td>
                                                                <td>
                                                                    {{ $callitem[3] }}
                                                                </td>
                                                                
                                                                
                                                            </tr> 
                                                        
                                                    @endforeach
                                                </tbody>
                                            
                                            </table>
                                        </div>
                                        <!--end: Datatable -->
                                    </div>
                                </div>
                            </div>

                    </div>
                    <div class="col-xl-6">
                            <div class="m-content">
                                <div class="m-portlet m-portlet--mobile">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h2 class="m-portlet__head-text">
                                                    <i class="la flaticon-statistics"></i>
                                                    Llamadas por Suscripción
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        <div class="btn-export-pdf">
                                            <a href="{{ url('RcallBySubscription') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar
                                                </button>
                                            </a>
                                            <a href="{{ url('RcallsOfSubscriptionByMonth') }}" target=_blank>    
                                                <button  type="btn" class="btn btn-primary">
                                                    Exportar por Mes
                                                </button>
                                            </a>
                                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                                        </div>	
                                        
                                        <!--end: Search Form -->
                                <!--begin: Datatable -->
                                       <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                            <table class="table table-striped table-hover" id="table-items" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            Suscripción
                                                        </th>
                                                        <th>
                                                            Cantidad de llamadas
                                                        </th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($listCallSuscription as $key => $callsuscription)
                                                    
                                                        <tr>
                                                            <td>
                                                                {{ $key}}
                                                            </td>
                                                            <td>
                                                                {{ $callsuscription}}
                                                            </td>
                                                            
                                                        </tr> 
                                                        
                                                    @endforeach
                                                </tbody>
                                            
                                            </table>
                                        </div>
                                        <!--end: Datatable -->
                                    </div>
                                </div>
                            </div>

                    </div>
        
                </div>
            </div>
        

       
        
@endsection


@section('script')
<script>
    var bandwidthChart1 = function() {
        var msg = 'dashboart'
        if ($('#call_by_month').length == 0) {
            return;
        }

        $.ajax({
            method: "GET",
            url: "/getCall/"+msg+"/",
            data: { '_token': '{{ csrf_token() }}' },
            })
            .done(function( data ) {
                console.log('data',data.months)
                console.log('total',data.total)
                $( ".totalCall" ).append( "<p>"+data.total+"</p>" );
                var January   = data.months[0]
                var February  = data.months[1]
                var March     = data.months[2]
                var April     = data.months[3]
                var May       = data.months[4]
                var June      = data.months[5]
                var July      = data.months[6]
                var August    = data.months[7]
                var September = data.months[8]
                var October   = data.months[9]
                var November  = data.months[10]
                var December  = data.months[11]

                $( ".callJanuary" ).append( "<p>"+"Ene: "+data.months[0]+"</p>" );
                $( ".callFebruary" ).append( "<p>"+"Feb: "+data.months[1]+"</p>" );
                $( ".callMarch" ).append( "<p>"+"Mar: "+data.months[2]+"</p>" );
                $( ".callApril" ).append( "<p>"+"Abr: "+data.months[3]+"</p>" );
                $( ".callMay" ).append( "<p>"+"May: "+data.months[4]+"</p>" );
                $( ".callJune" ).append( "<p>"+"Jun: "+data.months[5]+"</p>" );
                $( ".callJuly" ).append( "<p>"+"Jul: "+data.months[6]+"</p>" );
                $( ".callAugust" ).append( "<p>"+"Ago: "+data.months[7]+"</p>" );
                $( ".callSeptember" ).append( "<p>"+"Sep: "+data.months[8]+"</p>" );
                $( ".callOctober" ).append( "<p>"+"Oct: "+data.months[9]+"</p>" );
                $( ".callNovember" ).append( "<p>"+"Nov: "+data.months[10]+"</p>" );
                $( ".callDecember" ).append( "<p>"+"Dic: "+data.months[11]+"</p>" );

                var ctx = document.getElementById("call_by_month").getContext("2d");

                var gradient = ctx.createLinearGradient(0, 0, 0, 240);
                gradient.addColorStop(0, Chart.helpers.color('#d1f1ec').alpha(1).rgbString());
                gradient.addColorStop(1, Chart.helpers.color('#d1f1ec').alpha(0.3).rgbString());

                var config = {
                    type: 'line',
                    data: {
                        labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                        datasets: [{
                            label: "Bandwidth Stats",
                            backgroundColor: gradient,
                            borderColor: mUtil.getColor('success'),

                            pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                            pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                            pointHoverBackgroundColor: mUtil.getColor('danger'),
                            pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),

                            //fill: 'start',
                            data: [
                                January, February, March, April, May, June, July, August, September,October,November, December
                            ]
                        }]
                    },
                    options: {
                        title: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'nearest',
                            intersect: false,
                            position: 'nearest',
                            xPadding: 10,
                            yPadding: 10,
                            caretPadding: 10
                        },
                        legend: {
                            display: false
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                            xAxes: [{
                                display: false,
                                gridLines: false,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Month'
                                }
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: false,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Value'
                                },
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        },
                        elements: {
                            line: {
                                tension: 0.0000001
                            },
                            point: {
                                radius: 4,
                                borderWidth: 12
                            }
                        },
                        layout: {
                            padding: {
                                left: 0,
                                right: 0,
                                top: 10,
                                bottom: 0
                            }
                        }
                    }
                };

                var chart = new Chart(ctx, config);
                
        });
       
        
    }


</script>
@endsection