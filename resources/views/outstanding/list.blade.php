
@extends('master')

@section('stylus')


@endsection
@section('content_admin')
<div class="m-content">

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <i class="la flaticon-light"></i>
                    Destacados
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">				
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <label for="exampleSelect1">
            <strong> Perfil: </strong>
                
            </label>
            <select name="category" class="form-control m-input " id="category">
                        <option selected>Seleccionar</option>
                @foreach ($categories as $category)
                        <option value="{{ $category->id }}">
                            {{$category->name}}
                        </option>
                @endforeach

            </select>
        </div>

            
            <div class="m-section__content">
                <table class="table table-striped m-table">
                    <thead>
                        <tr>
                            <th style="width:200px">
                                Puesto
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Acciónes
                            </th>

                        </tr>
                    </thead>
                    <tbody id="OutstandingByCategory">
                        
                    </tbody>
                </table>
            </div>
        </div>
   
    </div>
</div>
</div>
        <div class="m-content">

            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                <i class="la flaticon-list-3"></i>
                                Marcar, destacados por categorías
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <!--begin: Search Form -->	
                    @if (session('typemsg'))
                            @if (session('typemsg') == 'success')
                                <div class="alert alert-success">
                                <strong><p>{{ session('message') }}</p></strong>
                                </div>
                            @endif
                            @if (session('typemsg') == 'error')
                                <div class="alert alert-danger">
                                <strong><p>{{ session('message') }}</p></strong>
                                </div>
                            @endif	
                    @endif
                <!-- <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state" method = "POST" action = "{{url('ListOutstanding')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                    <button type="submit" id="Save" class="btn btn-primary btn-outstanding">
                                        Actualizar Destacados
                                    </button>
                                <div class="m-separator m-separator--dashed d-xl-none"></div>
                            </div>
                        </div>
                    </div>				
                    
                    <!--end: Search Form -->
            <!--begin: Datatable -->
                    <div class="m-section__content">
                            <table class="table table-striped m-table">
                                <thead>
                                    <tr>
                                        <th>
                                            Nombre
                                        </th>
                                        <th>
                                            Destacado
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="ItemsByCategory">
                                    
                                </tbody>
                            </table>
                        </div>
               <!-- </form>  end: Datatable -->
                </div>
            </div>
        </div>



        <div class="modal" id="removeItemModal" tabindex="-1" role="dialog" aria-labelledby="removeItem" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="form-inline" id="formDeleteItem" method="POST" action="">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="idDeleteItem" id="idDeleteItem" value="">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="removeItem">
                        <i class="flaticon-warning-2"></i> Eliminar Rubro
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                    ¿Estás seguro de eliminar a <b id="b_item"></b>?
                    </p>
                    <span class="m--font-danger">Si lo eliminas, se perderan los datos del mismo.</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <i class="la la-close"></i>
                        Cerrar
                    </button>
                    <button type="submit" class="btn btn-danger">
                        <i class="la la-trash"></i> 
                        Eliminar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
            
@endsection

@section('script')
<script>
    

    $("#category").change(function(){
        $('#ItemsByCategory').empty();
        var id = $(this).val();
        var listItems = '';

        listOfOutstandding(id)
    
        $.get( "/"+id+"/get_itemByCategory", function( data ) {
            var list = data.ItemByCategory
            for (var i = 0; i < list.length; i++) {
                listItems += "<tr>" 
                                + "<th scope='row'>" 
                                    + list[i].name 
                                + "</th>"
                                + "<td>"
                                        +"<label class='m-checkbox m-checkbox--success'>"
                                            +"<input type='checkbox' class='chekItem' name='chekItem-"+list[i].outstanding+"' id='"+list[i].id+"'>"
                                            +"<span></span>"
                                        +"</label>"
                                +"</td>"
                            +"</tr>";
            }

            
            $('#ItemsByCategory').append(listItems);
            $("input[type=checkbox]").each(function(){
                var checkName = this.name
                valCheck = checkName.split("-",2)
                if (valCheck[1] == 1) {
                    $('#'+this.id).prop('checked', true);
                }
            });
        });
    });

    //Funcion que trae la lista de destacados

    function listOfOutstandding(id){
        $('#OutstandingByCategory').empty();
        var listOutstanding = '';
        var ListOutstanding = '';
        var ranking = 0;
        let Check = [];
        console.log(id)
    
        $.get( "/"+id+"/get_outstandingByCategory",function( data ) {
            ListOutstanding = data.outstanding
            for (var i = 0; i < ListOutstanding.length; i++) {
                if (ListOutstanding[i][0][0].order_of_outstanding == null) {
                    ranking = 'S/P';
                } else {
                    ranking = ListOutstanding[i][0][0].order_of_outstanding;
                }
                
                listOutstanding += "<tr>" 
                                    +"<th>"
                                        +"<div id='ranking-"+ListOutstanding[i][0][0].id +"'>"+ranking+"</div>"
                                        +"<input type='text' id='input-"+ListOutstanding[i][0][0].id +"'class='form-control m-input inputItem' value='"+ranking+"'></div>"
                                    +"</th>"
                                    +"<th scope='row'>" 
                                        + ListOutstanding[i][0][0].name 
                                    +"</th>"
                                    +"<th>"
                                        +"<a href='#' data-id='"+ListOutstanding[i][0][0].id +"' class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btnUpdateItem' title='Editar'>"
                                            +"<i class='la la-edit'></i>"
                                        +"</a>"
                                        +"<a href='#' id='success-"+ListOutstanding[i][0][0].id +"' data-id='"+ListOutstanding[i][0][0].id +"' data-category='"+id +"' class='m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill btnSuccessItem' title='Aceptar'>"
                                            +"<i class='la la-check'></i>"
                                        +"</a>"
                                        +"<a href='#' id='close-"+ListOutstanding[i][0][0].id +"' data-id='"+ListOutstanding[i][0][0].id +"' class='m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnCloseItem' title='Cacelar'>"
                                            +"<i class='la la-remove'></i>"
                                        +"</a>"
                                    +"</th>"
                                +"</tr>";

                    
            }
            
            $('#OutstandingByCategory').append(listOutstanding);
            $(".btnCloseItem").hide()
            $(".btnSuccessItem").hide()
            $(".inputItem").hide()
            
        });
    }

    $(".btn-outstanding").click(function(){
        let valCheck = [];
        //recorro los check
        $("input[type=checkbox]:checked").each(function(){
            valCheck.push(this.id);
        });

        var idCategory = $("#category").val();
    
        if (valCheck.length == 0) {
            valCheck = 0;
        }
    
        // console.log(valChecks);
        $.ajax({
            method: "GET",
            url: "/ListOutstanding/"+ valCheck + "/" + idCategory,
            data: { '_token': '{{ csrf_token() }}'},
            async:true,
            })
            .done(function( data ) {
                listOfOutstandding(idCategory)
            
            });

    });

    

    $('table').delegate('.btnUpdateItem', 'click', function(){
        
        var id = $(this).data('id')

        $('#input-'+id).show()
        $('#success-'+id).show()
        $('#close-'+id).show()
        $('#ranking-'+id).hide()
        $('.btnUpdateItem').hide()

    });
    
    $('table').delegate('.btnCloseItem', 'click', function(){
        
        var id = $(this).data('id')

        $('#input-'+id).hide()
        $('#success-'+id).hide()
        $('#close-'+id).hide()
        $('#ranking-'+id).show()
        $('.btnUpdateItem').show()

    });
    
    $('table').delegate('.btnSuccessItem', 'click', function(){
        
        var idItem = $(this).data('id')
        var idCategory = $(this).data('category')
        var newRanking = $('#input-'+idItem).val()
        console.log(idCategory,'datosfuera')
        $.ajax({
            method: "GET",
            url: "/SaveRanking/"+ idItem + "/" + newRanking,
            data: { '_token': '{{ csrf_token() }}'},
            async:true,
            })
            .done(function( data ) {
                console.log(idCategory,'datosdentro')
                listOfOutstandding(idCategory)
            
            });

      
        $('#input-'+idItem).hide()
        $('#success-'+idItem).hide()
        $('#close-'+idItem).hide()
        $('#ranking-'+idItem).show()
        $('.btnUpdateItem').show()

	});

    


</script>
		
@endsection
                                    