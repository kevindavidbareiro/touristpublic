
@extends('master')

@section('stylus')
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}"/>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link rel="stylesheet" href="{{asset('css/fontawesome-stars.css')}}">
@endsection
@section('content_admin')


       <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Nuevo Item
                            </h3>
                        </div>
                    </div>
                </div>
                        
            
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state" method = "POST" action = "{{url('Item')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="m-portlet__body">
                        @if (session('typemsg'))
                            @if (session('typemsg') == 'success')
                                <div class="alert alert-success">
                                <strong><p>{{ session('message') }}</p></strong>
                                </div>
                            @endif
                            @if (session('typemsg') == 'error')
                                <div class="alert alert-danger">
                                <strong><p>{{ session('message') }}</p></strong>
                                </div>
                            @endif	
                        @endif	
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4 @if($errors->has('name')) has-danger @endif">
                                <label>
                                    <strong> Nombre: </strong> 
                                </label>
                                <input type="text" name="name" id="Name" class="form-control m-input @if($errors->has('name')) form-control-danger @endif" placeholder="Nombre" value="{{ old('name') }}">
                                
                                @if ($errors->any())
                                    @if($errors->has('name'))
                                    <div class="form-control-feedback">
                                        Por favor ingrese un nombre
								    </div>
                                    @endif
                                @endif
                            </div>
                            <div class="col-lg-4 @if($errors->has('address')) has-danger @endif">
                                <label>
                                <strong> Dirección: </strong>
                                    
                                </label>
                                <input type="textbox" name="address" id="address" class="form-control m-input form-control-danger @if($errors->has('address')) form-control-danger @endif" placeholder="Dirección" value="{{ old('address') }}">
                                @if ($errors->any())
                                    @if($errors->has('address'))
                                    <div class="form-control-feedback">
                                        Por favor ingrese una dirección
								    </div>
                                    @endif
                                @endif
                            </div>
                            <div class="col-lg-4 btn-busca-map">
                                
                                <input id="submit" class="btn btn-primary" type="button" value="Buscar">
                            </div>
                            <div class="col-lg-12" style="margin: 10px 0 0 0">
                                <label>
                                    <strong> Keywords: </strong>
                                </label>
                                <input type="textbox" name="keywords" id="keywords" class="form-control m-input" placeholder="ej.: pizza, hamburguesa, comer" value="{{ old('keywords') }}">
                            </div>
                           
                        </div>
                        <div class="form-group m-form__group row">
                            <div id="map"></div>
                            <div class="col-lg-6 @if($errors->has('latitude')) has-danger @endif">
                                <label>
                                    <strong> Latitud: </strong> 
                                </label>
                                <input type="number" step="any" name="latitude" id="latitude" class="form-control m-input @if($errors->has('latitude')) form-control-danger @endif" placeholder="Latitud" value="{{ old('latitude') }}">
                                @if ($errors->any())
                                    @if($errors->has('latitude'))
                                    <div class="form-control-feedback">
                                        Por favor ingrese una Latitud
								    </div>
                                    @endif
                                @endif
                            </div>
                            <div class="col-lg-6 @if($errors->has('longitude')) has-danger @endif">
                                <label>
                                    <strong> Longitud: </strong> 
                                </label>
                                <input type="number" step="any" name="longitude" id="longitude" class="form-control m-input @if($errors->has('longitude')) form-control-danger @endif" placeholder="Longitud" value="{{ old('longitude') }}">
                                @if ($errors->any())
                                    @if($errors->has('longitude'))
                                    <div class="form-control-feedback">
                                        Por favor ingrese una Longitud
								    </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('phone')) has-danger @endif">
                                <label>
                                    <strong> Teléfono: </strong> Ingrese solo números sin "-" ni "()".
                                        
                                </label>
                                <input type="tel" name="phone" id="Phone" class="form-control m-input @if($errors->has('phone')) form-control-danger @endif" placeholder="Ej: 3704719836" value="{{ old('phone') }}" pattern="[0-9]{10}">
                                @if ($errors->any())
                                    @if($errors->has('phone'))
                                    <div class="form-control-feedback">
                                        Por favor ingrese un teléfono
								    </div>
                                    @endif
                                @endif
                                    

                            </div>
                            <div class="col-lg-6">
                                <label for="exampleSelect1">
                                    <strong> Perfil: </strong>
                                        
                                    </label>
                                    <select name="category" class="form-control m-input " id="category">

                                        @foreach ($categories as $category)
                                                <option value="{{ $category->id }}" @if ($category->id == 1) selected @endif
                                                    @if (old('category'))
                                                        @if (old('category') == $category->id)
                                                            selected
                                                        @endif
                                                    @endif
                                                    >
                                                    {{$category->name}}
                                                </option>
                                        @endforeach

                                    </select>
                            </div>
                            
                            <div class="col-lg-6" id="divSubCategory">
                                    <label for="exampleSelect1">
                                        <strong> Categoría: </strong>
                                        
                                    </label>
                                    <select class="form-control m-input m-input--solid" name="SubCategory" id="SubCategory">
									
										
								</select> 
                            </div>
                            <div class="col-lg-6" id="netAtms">
                                    <label for="exampleSelect1">
                                        <strong> Red: </strong>
                                        
                                    </label>
                                    <select name="netAtms" class="form-control m-input">
                                        <option value="1" selected>link</option>
                                        <option value="2" >Banelco</option>
                                    </select>    
                            </div>
                        			
                        </div>
                        <div id="divStar" class="form-group m-form__group row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                            <input id="nstar" name="nstar" value="1">
                                <center>
                                    <label for="exampleSelect1">
                                        <strong> Estrellas </strong>  
                                    </label>
                                    <select id="star">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </center>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 ">
                                <label>
                                    <strong> Facebook: </strong> 
                                </label>
                                <input type="text" name="facebook" id="Facebook" class="form-control m-input " placeholder="Facebook" value="{{ old('facebook') }}">
                                
                                
                            </div>
                            <div class="col-lg-6 ">
                                <label>
                                <strong> Instagram: </strong>
                                    
                                </label>
                                <input type="textbox" name="instagram" id="Instagram" class="form-control m-input form-control-danger" placeholder="Instagram" value="{{ old('instagram') }}">
                               
                            </div>
                            <div class="col-lg-6">
                                <label>
                                <strong> Twitter: </strong>
                                    
                                </label>
                                <input type="textbox" name="twitter" id="Twitter" class="form-control m-input form-control-danger" placeholder="Twitter" value="{{ old('twitter') }}">
                               
                            </div>
                            <div class="col-lg-6">
                                <label>
                                <strong> Sitio Web: </strong>
                                    
                                </label>
                                <input type="textbox" name="web" id="Web" class="form-control m-input form-control-danger" placeholder="Web" value="{{ old('web') }}">
                               
                            </div>
                            
                           
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-12 @if($errors->has('description')) has-danger @endif">
                                <label for="description">
                                <strong>Descripción</strong>
                                </label>

                                <textarea name="description" class="form-control m-input @if($errors->has('description')) form-control-danger @endif" rows="3" value="{{ old('description') }}"> </textarea>
                                @if ($errors->any())
                                    @if($errors->has('description'))
                                    <div class="form-control-feedback">
                                        Por favor ingrese una descrpción
								    </div>
                                    @endif
                                @endif    
                                <button type="button" id="traslate" class="btn btn-primary btn-traslate">
                                        Traducir al inglés
                                </button> 
                                                       
                            </div>
                            
                        </div>

                        <div id="descriptionEn" class="form-group m-form__group row">
                            <div class="col-lg-12">
                                <label for="description">
                                <strong>Traducir la Descripción al inglés</strong>
                                </label>
                            
                                <textarea name="descriptionEn" id="textDescriptionEn"class="form-control m-input " rows="3" value="{{ old('descriptionEn') }}"> </textarea>
                                <button type="button" id="cancelEn" class="btn btn-primary btn-traslate">
                                        Cancelar
                                </button>       
                            </div>
                        </div>
                        <div id="subscription" class="form-group m-form__group row">
                            <div class="col-lg-6">
                                <label for="description">
                                <strong>Suscripción</strong>
                                </label>
                                <select name="subscription" class="form-control m-input " id="subscription">
     
                                    <option value="1" selected>Básico</option>
                                    <option value="2" >SemiPremium</option>
                                    <option value="3" >Premium</option>
                                       

                                </select>    
                            </div>
                            
                        </div>

                       
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-8">
                                    <button type="submit" id="Save" class="btn btn-primary">
                                        Guardar
                                    </button>
                                    <a href="{{url('Item')}}" class="btn btn-secondary">
                                       <span>
                                            Volver
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                 </div>
            </form>
               
               
        </div>

    
   
        
@endsection

@section('script')
<script src="{{asset('templante/metronic/default/assets/demo/default/custom/components/forms/widgets/nouislider.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.barrating.min.js')}}"></script>
<script type="text/javascript">
   $(function() {
      $('#star').barrating({
        theme: 'fontawesome-stars'
      });
      $("#star").change(function(){
            var star = $('#star option:selected').val()
            $('#nstar').val(star)            
    });
   });
</script>
@endsection

@section('scriptmaps')
        <script>
                var markers = [];
                function initMap() {
                    //Inicio del mapa y colocando en una direccion determinada
                    var map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: -26.185164, lng: -58.174386},
                    zoom: 15
                    });
               

                    //Seccion para autocompletar las direcciones
                    var input = document.getElementById('address');
                    var autocomplete = new google.maps.places.Autocomplete(input);
                    autocomplete.bindTo('bounds', map);
                    autocomplete.setComponentRestrictions({'country': ['ar']});


                    //Inicio del geocoding
                    var geocoder = new google.maps.Geocoder();
                    

                    document.getElementById('submit').addEventListener('click', function() {
                    geocodeAddress(geocoder, map);
                    });
                }

                function geocodeAddress(geocoder, resultsMap) {
                    
                    var address = document.getElementById('address').value;
                    geocoder.geocode({'address': address}, function(results, status) {
                   
                    if (status === 'OK') {
                        resultsMap.setCenter(results[0].geometry.location);
                        //ELimino los marcadores
                        deleteMarkers();
                        //Creo un marcador de la direccion que tipeo el usuario
                        var marker = new google.maps.Marker({
                        map: resultsMap,
                        position: results[0].geometry.location
                        });
                        markers.push(marker);
                        //Muestro las cordenadas de latitud y longitud en los inpus correspondientes
                        document.getElementById("latitude").value = results[0].geometry.location.lat();
                        document.getElementById("longitude").value = results[0].geometry.location.lng();
                    } else {
                        alert('Geocode no tuvo éxito por la siguiente razón: ' + status);
                    }
                    });
                }
                function setMapOnAll(map) {
                    for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                    }
                }
                function clearMarkers() {
                    setMapOnAll(null);
                } 
                function deleteMarkers() {
                    clearMarkers();
                    markers = [];
                }


        </script>
         <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpenSSGiH5wkPhx_tckk2yev_Je87j4aU&libraries=places&callback=initMap">
        </script>
        
  
        <script>
                $(document).ready(function(){
                    var id = $('#category').val();
                    var listItems = '';
                    getSubcategory(id,listItems)
                    $('#descriptionEn').hide();
                    $('#netAtms').hide();
                    $('#nstar').hide();
                    $('#divStar').hide();
                   
                    
                    
                    $("#category").change(function(){
                        var perfil = $('#category option:selected').val()
                        var id = $(this).val();
                        var listItems = '';
                        getSubcategory(id,listItems)
                        if (perfil == 7) {
                            $('#netAtms').show();
                        }else{
                            $('#netAtms').hide();
                        }
                        if (perfil == 3) {
                            $('#divStar').show();
                        }else{
                            $('#divStar').hide();
                        }
                    });

                   
                    $("#traslate").click(function(){
			            $('#descriptionEn').show(500);
			
		            });

                    $("#cancelEn").click(function(){
			            $('#descriptionEn').hide(500);
                        $('#textDescriptionEn').val('');
			
		            });


                    function getSubcategory(id,listItems){
                        $.get( "/"+id+"/get_subcategory", function( data ) {
                            var list = data.SubCategory;
                            for (var i = 0; i < list.length; i++) {
                                listItems += "<option value='" + list[i].id + "'>" + list[i].name + "</option>";
                            }
                            
                            if (listItems != '') {
                                $('#divSubCategory').show();
                            }else{
                                $('#divSubCategory').hide();
                            }
                            $('#SubCategory').html(listItems);
                        });

                        return 'ok'
                    }
            
                });

        </script>
        
@endsection