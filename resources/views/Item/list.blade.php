
@extends('master')

@section('stylus')


@endsection
@section('content_admin')
        
        <div class="m-content">

            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                <i class="la flaticon-signs"></i>
                                Item
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <!--begin: Search Form -->	
                    @if (session('typemsg'))
                            @if (session('typemsg') == 'success')
                                <div class="alert alert-success">
                                <strong><p>{{ session('message') }}</p></strong>
                                </div>
                            @endif
                            @if (session('typemsg') == 'error')
                                <div class="alert alert-danger">
                                <strong><p>{{ session('message') }}</p></strong>
                                </div>
                            @endif	
                    @endif				
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input m-input--solid" placeholder="Buscar..." id="generalSearch">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                <span>
                                                    <i class="la la-search"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                <a href="{{url('Item/create')}}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                    <span>
                                        <i class="la flaticon-signs"></i>
                                        <span>
                                            Nuevo
                                        </span>
                                    </span>
                                </a>
                                <div class="m-separator m-separator--dashed d-xl-none"></div>
                            </div>
                        </div>
                    </div>
                    <!--end: Search Form -->
            <!--begin: Datatable -->
            
                    <table class="m-datatable" id="table-competitors" width="100%">
                        <thead>
                            <tr>
                                <th>
                                    Nombre
                                </th>
                                <th>
                                    Dirección
                                </th>
                                <th>    
                                    Categoria
                                </th>
                                <th>    
                                    Suscripción
                                </th>
                                <th>    
                                    Multimedia
                                </th>
                                <th>    
                                    Acción
                                </th>
                                
                            </tr>
                        </thead>
                        <tbody>
                             @foreach($listItem as $item)
                                    <tr>
                                        <td>
                                            {{ $item->name }}
                                        </td>
                                        <td>
                                            {{ $item->address }}
                                        </td>
                                        <td>
                                            {{ $item->category->name }}
                                        </td>
                                        <td>
                                            @if($item->subscription == 1)<span>Básico</span>@endif
                                            @if($item->subscription == 2)<span>SemiPremium</span>@endif
                                            @if($item->subscription == 3)<span>Premium</span>@endif
                                        
                                        </td>
                                       
                                        <td>
                                            <a
                                                href="{{url('Multimedia/'.$item->id.'')}}"
                                                class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill btnUpdateItem"
                                                title="Cargar ">
                                                <i class="la la-image"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a
                                                href="{{url('Item/'.$item->id.'/edit')}}"
                                                class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btnUpdateItem"
                                                title="Editar ">
                                                <i class="la la-edit"></i>
                                            </a>
                                            <a
                                                href="#"
                                                data-id="{{ $item->id }}" 
                                                data-name="{{ $item->name }}" 
                                                class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btnDeleteItem"
                                                title="Eliminar">
                                                <i class="la la-trash"></i>
                                            </a>
                                        </td>
                                    </tr>   
                            @endforeach
                        </tbody>
                    
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>
        </div>



        <div class="modal" id="removeItemModal" tabindex="-1" role="dialog" aria-labelledby="removeItem" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="form-inline" id="formDeleteItem" method="POST" action="">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="idDeleteItem" id="idDeleteItem" value="">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="removeItem">
                        <i class="flaticon-warning-2"></i> Eliminar Rubro
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                    ¿Estás seguro de eliminar a <b id="b_item"></b>?
                    </p>
                    <span class="m--font-danger">Si lo eliminas, se perderan los datos del mismo.</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <i class="la la-close"></i>
                        Cerrar
                    </button>
                    <button type="submit" class="btn btn-danger">
                        <i class="la la-trash"></i> 
                        Eliminar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
            
@endsection

@section('script')
<script>
    $('table').delegate('.btnDeleteItem', 'click', function(){
			$("#formDeleteItem").attr("action", '/Item/' + $(this).data('id'));
			$('#idDeleteItem').val($(this).data('id'));
			$('#b_item').text($(this).data('name'));
			$('#removeItemModal').modal('show');
		});
</script>
		
@endsection