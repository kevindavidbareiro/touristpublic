
@extends('master')

@section('stylus')
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}"/>

@endsection
@section('content_admin')
    <div class="m-subheader ">
          <div class="d-flex align-items-center">
              <div class="mr-auto">
                  <h3 class="m-subheader__title ">
                    {{$item}}
                  </h3>
              </div>
          </div>
		</div>
    <div class="m-portlet">
        <div class="m-portlet__body  m-portlet__body--no-padding">
			      <div class="row m-row--no-padding m-row--col-separator-xl">
                  <input type="hidden" id="idItem" name="id" value="{{$id}}">
                  <div class="col-md-12 col-lg-6 ">
                      <!--begin::New Users-->
                      <div class="m-widget24">
                          <div class="m-widget24__item">
                              <h4 class="m-widget24__title">
                                Cantidad de Visitas totales
                              </h4>
                              <br>
                              
                              <span class=" m--font-success numberDashboard">
                                {{$SearcheOfItem}}
                              </span>
                              <div class="m--space-10"></div>
                            
                          </div>
                      </div>
                                <!--end::New Users-->
                  </div>

                  <div class="col-md-12 col-lg-6 ">
                      <!--begin::New Users-->
                      <div class="m-widget24">
                          <div class="m-widget24__item">
                              <h4 class="m-widget24__title">
                                Cantidad de llamadas totales
                              </h4>
                              <br>
                              
                              <span class=" m--font-success numberDashboard">
                                {{$CallOfItem}}
                              </span>
                              <div class="m--space-10"></div>
                            
                          </div>
                      </div>
                      <!--end::New Users-->
									</div>


                  <div class="col-md-12 col-lg-6 ">
                      <!--begin::New Users-->
                      <div class="m-widget24">
                          <div class="m-widget24__item">
                              <h4 class="m-widget24__title">
                                Cantidad de visitas en {{$month}} 
                              </h4>
                              <br>
                              
                              <span class=" m--font-success numberDashboard">
                                {{$SearcheOfMonth}}
                              </span>
                              <div class="m--space-10"></div>
                            
                          </div>
                      </div>
                      <!--end::New Users-->
									</div>

                  <div class="col-md-12 col-lg-6 ">
                      <!--begin::New Users-->
                      <div class="m-widget24">
                          <div class="m-widget24__item">
                              <h4 class="m-widget24__title">
                                Cantidad de llamas en {{$month}} 
                              </h4>
                              <br>
                              
                              <span class=" m--font-success numberDashboard">
                                {{$CallOfMonth}}
                              </span>
                              <div class="m--space-10"></div>
                            
                          </div>
                      </div>
                      <!--end::New Users-->
									</div>
                  
							</div>
				</div>
      </div>

      <div class="m-portlet">
        <div class="m-portlet__body">
            <div id="chart">
                
            </div>
        </div>
    </div>

    <div class="m-portlet">
        <div class="m-portlet__body">
            <div id="chartCall">
                
            </div>
        </div>
    </div>

    <div class="m-portlet">
        <div class="m-portlet__body">
            <div id="chartColumn">
                
            </div>
        </div>
    </div>

    <div class="m-portlet">
        <div class="m-portlet__body">
            <div id="chartColumnCall">
                
            </div>
        </div>
    </div>

       
        
@endsection


@section('script')
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script>

      $( document ).ready(function() {
          
          var idItem = $('#idItem').val()

          $.ajax({
              type: 'GET',
              url: '/searche7days/'+ idItem,
              dataType: 'json',
              data: {
                "_token": "{{ csrf_token() }}",
              },
              success: function (data) {
                  console.log(data)
                  var options = {
                  series: [{
                    name: "Visitas",
                    data: data.days
                  }],
                    chart: {
                    height: 350,
                    type: 'line',
                    zoom: {
                      enabled: false
                    },
                    locales:[{
                      "name":"es",
                      "options":{
                        "toolbar":{
                          "exportToSVG": "Descargar SVG",
                          "exportToPNG": "Descargar PNG",
                          "exportToCSV": "Descargar CSV",
                        }
                      }
                    }],
                    defaultLocale: "es"
                  },
                  dataLabels: {
                    enabled: false
                  },
                  stroke: {
                    curve: 'straight'
                  },
                  title: {
                    text: 'Visitas últimos 7 días',
                    align: 'left'
                  },
                  grid: {
                    row: {
                      colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                      opacity: 0.5
                    },
                  },
                  xaxis: {
                    
                    categories: data.week,
                  }
                  };

                  var chart = new ApexCharts(document.querySelector("#chart"), options);
                  chart.render();
                    
              },
              error: function (data) {
                
              }
			    });


           $.ajax({
              type: 'GET',
              url: '/call7days/'+ idItem,
              dataType: 'json',
              data: {
                "_token": "{{ csrf_token() }}",
              },
              success: function (data) {
                  
                  var options = {
                  series: [{
                    name: "Visitas",
                    data: data.Calldays
                  }],
                    chart: {
                    height: 350,
                    type: 'line',
                    zoom: {
                      enabled: false
                    },
                    locales:[{
                      "name":"es",
                      "options":{
                        "toolbar":{
                          "exportToSVG": "Descargar SVG",
                          "exportToPNG": "Descargar PNG",
                          "exportToCSV": "Descargar CSV",
                        }
                      }
                    }],
                    defaultLocale: "es"
                  },
                  dataLabels: {
                    enabled: false,
                    formatter: function (val) {
                        return val ;
                    },
                  },
                  stroke: {
                    curve: 'straight'
                  },
                  title: {
                    text: 'LLamadas de los últimos 7 días',
                    align: 'left'
                  },
                  grid: {
                    row: {
                      colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                      opacity: 0.5
                    },
                  },
                  xaxis: {
                    
                    categories: data.Callweek,
                  }
                  };

                  var chartCall = new ApexCharts(document.querySelector("#chartCall"), options);
                  chartCall.render();
                    
              },
              error: function (data) {
                
              }
			    });	

            $.ajax({
              type: 'GET',
              url: '/yearForMonth/'+ idItem,
              dataType: 'json',
              data: {
                "_token": "{{ csrf_token() }}",
              },
              success: function (data) {
                console.log(data)
                  var options = {
                      series: [{
                      name: 'Visitas',
                      data: data.MonthForYears
                    }],
                      chart: {
                      height: 350,
                      type: 'bar',
                      locales:[{
                        "name":"es",
                        "options":{
                          "toolbar":{
                            "exportToSVG": "Descargar SVG",
                            "exportToPNG": "Descargar PNG",
                            "exportToCSV": "Descargar CSV",
                          }
                        }
                      }],
                      defaultLocale: "es"
                    },
                    plotOptions: {
                      bar: {
                        dataLabels: {
                          position: 'top', // top, center, bottom
                        },
                      }
                    },
                    dataLabels: {
                      enabled: true,
                      formatter: function (val) {
                        return val ;
                      },
                      offsetY: -30,
                      style: {
                        fontSize: '12px',
                        colors: ["#304758"]
                      }
                    },
                    
                    xaxis: {
                      categories: ["Ene", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                      position: 'top',
                      axisBorder: {
                        show: false
                      },
                      axisTicks: {
                        show: false
                      },
                      crosshairs: {
                        fill: {
                          type: 'gradient',
                          gradient: {
                            colorFrom: '#D8E3F0',
                            colorTo: '#BED1E6',
                            stops: [0, 100],
                            opacityFrom: 0.4,
                            opacityTo: 0.5,
                          }
                        }
                      },
                      tooltip: {
                        enabled: true,
                      }
                    },
                    yaxis: {
                      axisBorder: {
                        show: false
                      },
                      axisTicks: {
                        show: false,
                      },
                      labels: {
                        show: false,
                        formatter: function (val) {
                          return val;
                        }
                      }
                    
                    },
                    title: {
                      text: 'Vistas por mes',
                      floating: true,
                      offsetY: 330,
                      align: 'center',
                      style: {
                        color: '#444'
                      }
                    }
                    };

                    var chartColumn = new ApexCharts(document.querySelector("#chartColumn"), options);
                    chartColumn.render();
                 
                    
              },
              error: function (data) {
                
              }
			    });	


          $.ajax({
              type: 'GET',
              url: '/yearForMonthCall/'+ idItem,
              dataType: 'json',
              data: {
                "_token": "{{ csrf_token() }}",
              },
              success: function (data) {
                console.log(data)
                  var options = {
                      series: [{
                      name: 'LLamadas',
                      data: data.MonthForYearsCall
                    }],
                      chart: {
                      height: 350,
                      type: 'bar',
                      locales:[{
                        "name":"es",
                        "options":{
                          "toolbar":{
                            "exportToSVG": "Descargar SVG",
                            "exportToPNG": "Descargar PNG",
                            "exportToCSV": "Descargar CSV",
                          }
                        }
                      }],
                      defaultLocale: "es"
                    },
                    plotOptions: {
                      bar: {
                        dataLabels: {
                          position: 'top', // top, center, bottom
                        },
                      }
                    },
                    dataLabels: {
                      enabled: true,
                      formatter: function (val) {
                        return val ;
                      },
                      offsetY: -30,
                      style: {
                        fontSize: '12px',
                        colors: ["#304758"]
                      }
                    },
                    
                    xaxis: {
                      categories: ["Ene", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                      position: 'top',
                      axisBorder: {
                        show: false
                      },
                      axisTicks: {
                        show: false
                      },
                      crosshairs: {
                        fill: {
                          type: 'gradient',
                          gradient: {
                            colorFrom: '#D8E3F0',
                            colorTo: '#BED1E6',
                            stops: [0, 100],
                            opacityFrom: 0.4,
                            opacityTo: 0.5,
                          }
                        }
                      },
                      tooltip: {
                        enabled: true,
                      }
                    },
                    yaxis: {
                      axisBorder: {
                        show: false
                      },
                      axisTicks: {
                        show: false,
                      },
                      labels: {
                        show: false,
                        formatter: function (val) {
                          return val;
                        }
                      }
                    
                    },
                    title: {
                      text: 'LLamadas por mes',
                      floating: true,
                      offsetY: 330,
                      align: 'center',
                      style: {
                        color: '#444'
                      }
                    }
                    };

                    var chartColumnCall = new ApexCharts(document.querySelector("#chartColumnCall"), options);
                    chartColumnCall.render();
                 
                    
              },
              error: function (data) {
                
              }
			    });


       
      });

</script>
@endsection