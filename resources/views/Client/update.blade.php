
@extends('master')

@section('stylus')


@endsection
@section('content_admin')

       <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Editar Cliente
                            </h3>
                        </div>
                    </div>
                </div>
                
            
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state" method = "POST" action = "{{url('Client/'.$client->id.'/')}}">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="m-portlet__body">
                        @if (session('typemsg'))
                            @if (session('typemsg') == 'success')
                                <div class="alert alert-success">
                                <strong><p>{{ session('message') }}</p></strong>
                                </div>
                            @endif
                            @if (session('typemsg') == 'error')
                                <div class="alert alert-danger">
                                <strong><p>{{ session('message') }}</p></strong>
                                </div>
                            @endif	
                        @endif		
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('name')) has-danger @endif">
                                <label>
                                    <strong> Nombre: </strong> 
                                </label>
                                <input type="text" name="name" id="Name" class="form-control m-input @if($errors->has('name')) form-control-danger @endif" placeholder="Nombre" @if (old('name')) value="{{ old('name') }}" @else value="{{ $client->user->name }}"@endif>
                                
                                @if ($errors->any())
                                    @if($errors->has('name'))
                                    <div class="form-control-feedback">
                                        Por favor ingrese un nombre
								    </div>
                                    @endif
                                @endif
                            </div>
                            <div class="col-lg-6 @if($errors->has('lastname')) has-danger @endif">
                                <label>
                                <strong> Apellido: </strong>
                                    
                                </label>
                                <input type="text" name="lastname" id="LastName" class="form-control m-input form-control-danger @if($errors->has('lastname')) form-control-danger @endif" placeholder="Apellido" @if (old('lastname')) value="{{ old('lastname') }}" @else value="{{ $client->user->lastname }}"@endif>
                                @if ($errors->any())
                                    @if($errors->has('lastname'))
                                    <div class="form-control-feedback">
                                        Por favor ingrese un apellido
								    </div>
                                    @endif
                                @endif
                            </div>
                            
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('email')) has-danger @endif">
                                <label>
                                    <strong> Email: </strong> 
                                </label>
                                <input type="email" name="email" id="Email" class="form-control m-input @if($errors->has('email')) form-control-danger @endif" placeholder="Email" @if (old('email')) value="{{ old('email') }}" @else value="{{ $client->user->email }}"@endif>
                                @if ($errors->any())
                                    @if($errors->has('email'))
                                    <div class="form-control-feedback">
                                        Por favor ingrese un correo electronico
								    </div>
                                    @endif
                                    @if($errors->has('email'))
                                        <div class="form-control-feedback">
                                            O el correo electronico ya existe ingrese otro porfavor
                                        </div>
                                    @endif
                                @endif
                            </div>
                             <div class="col-lg-6">
                                <label>
                                <strong> Contraseña: </strong>
                                    
                                </label>
                                <input type="password" name="password" id="Password" class="form-control m-input" placeholder="Contraseña" value="">
                                
                            </div>
                        </div>

                       
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6">
                            <label for="exampleSelect1">
                                <strong> Empresa: </strong>
                                    
                                </label>
                            
                                <select class="form-control m-select2" id="m_select2_1" name="item">

                                    @foreach ($items as $item)
                                            <option value="{{ $item->id }}"
                                            
                                                @if (old('item'))
                                                        @if (old('item') == $item->id)
                                                            selected
                                                        @endif
                                                @elseif ($client->item_id == $item->id)
                                                        selected
                                                @endif
                                                >
                                                {{ $item->name }}
                                            </option>
                                    @endforeach

                                
                                </select>

                            </div>
                            
                        </div>
                     

                       
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-8">
                                    <button type="submit" id="Save" class="btn btn-primary">
                                        Guardar
                                    </button>
                                    <a href="{{url('Client')}}" class="btn btn-secondary">
                                       <span>
                                            Volver
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                 </div>
                </form>
                <!--end::Form-->
               
        </div>
@endsection



@section('scriptSelect2')
<script src="{{asset('templante/metronic/default/assets/demo/default/custom/components/forms/widgets/select2.js')}}" type="text/javascript"></script>


@endsection