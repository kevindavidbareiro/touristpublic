
@extends('master')

@section('stylus')


@endsection
@section('content_admin')

       <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Editar Usuario
                            </h3>
                        </div>
                    </div>
                </div>
                
            
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state" method = "POST" action = "{{url('updateRolsdeUsuarios/'.$user->id.'/')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="m-portlet__body">
                        @if (session('typemsg'))
                            @if (session('typemsg') == 'success')
                                <div class="alert alert-success">
                                <strong><p>{{ session('message') }}</p></strong>
                                </div>
                            @endif
                            @if (session('typemsg') == 'error')
                                <div class="alert alert-danger">
                                <strong><p>{{ session('message') }}</p></strong>
                                </div>
                            @endif	
                        @endif		
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6 @if($errors->has('name')) has-danger @endif">
                                <label>
                                    <strong> Nombre: {{$user->name}}</strong> 
                                </label>
                                
                            </div>
                            <div class="col-lg-6 @if($errors->has('email')) has-danger @endif">
                                <label>
                                    <strong> Email: {{$user->email}}</strong> 
                                </label>
                               
                            </div>
                            <div class="col-lg-6 @if($errors->has('email')) has-danger @endif">
                                <label>
                                    <strong> rol: {{$user->rol_id}}</strong> 
                                </label>
                               
                            </div>
                           
                            
                        </div>
                       
                        
                   
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                <label for="exampleSelect1">
                                    <strong> Rol: </strong>
                                        
                                    </label>
                                
                                    <select name="rol" class="form-control m-input m-input--air " id="Rol">

                                        @foreach ($rols as $rol)
                                                <option value="{{ $rol->id }}">
                                                    {{ $rol->name }}
                                                </option>
                                        @endforeach

                                    
                                    </select>

                                </div>
                                
                            </div>
                           

                       
                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions--solid">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-8">
                                    <button type="submit" id="Save" class="btn btn-primary">
                                        Guardar
                                    </button>
                                    <a href="{{url('updateRolsdeUsuarios')}}" class="btn btn-secondary">
                                       <span>
                                            Volver
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                 </div>
                </form>
                <!--end::Form-->
               
        </div>
@endsection


@section('script')

@endsection