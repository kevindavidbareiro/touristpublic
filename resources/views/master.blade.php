<!DOCTYPE html>

<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/mm,.etronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<?php
			use App\Http\Helpers\Helpers;

			$url = Request::path();// devuelve url
			$enlace = Helpers::enlaceActivo($url);




		?>
		<meta charset="utf-8" />
		<title>
			iruTv | Panel
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		<!--end::Web font -->
        <!--begin::Base Styles -->  
        <!--begin::Page Vendors -->
		<link href="{{asset('templante/metronic/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors -->
		<link href="{{asset('templante/metronic/default/assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('templante/metronic/default/assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<!--end::Base Styles -->
		<!--<link rel="shortcut icon" href="{{asset('templante/metronic/default/assets/demo/default/media/img/logo/favicon.ico')}}" /> -->
		@yield('stylus')
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body style="" class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<!-- BEGIN: Header -->
			<header class="m-grid__item    m-header "  data-minimize-offset="200" data-minimize-mobile-offset="200" >
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop">
						<!-- BEGIN: Brand -->
						<div class="m-stack__item m-brand  m-brand--skin-dark ">
							<div class="m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-stack__item--middle m-brand__logo">
									<a href="index.html" class="m-brand__logo-wrapper">
									
									  	<!--  <img alt="" src="{{asset('templante/metronic/default/assets/demo/default/media/img/logo/logo_default_dark.png')}}"/>  -->
									
									</a>
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">
									<!-- BEGIN: Left Aside Minimize Toggle -->
									<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block 
					 ">
										<span></span>
									</a>
									<!-- END -->
							<!-- BEGIN: Responsive Aside Left Menu Toggler -->
									<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>
									<!-- END -->
							
			<!-- BEGIN: Topbar Toggler -->
									<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-more"></i>
									</a>
									<!-- BEGIN: Topbar Toggler -->
								</div>
							</div>
						</div>
						<!-- END: Brand -->
						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
							<!-- BEGIN: Horizontal Menu -->
							<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn">
								<i class="la la-close"></i>
							</button>
							<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark "  >
								<ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
									
								</ul>
							</div>
							<!-- END: Horizontal Menu -->								<!-- BEGIN: Topbar -->
							<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-topbar__nav-wrapper">
									<ul class="m-topbar__nav m-nav m-nav--inline">
										
										<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__userpic">
													
													<img src="{{asset('templante/iconouser.png')}}" class="m--img-rounded m--marginless m--img-centered" alt=""/> 

												</span>
												<span class="m-topbar__username m--hide">
													Nick
												</span>
											</a>
											
											<div class="m-dropdown__wrapper">
												<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
												<div class="m-dropdown__inner">
													<div class="m-dropdown__header m--align-center" style="background: url(assets/app/media/img/misc/user_profile_bg.jpg); background-size: cover;">
														<div class="m-card-user m-card-user--skin-dark">
															<!--<div class="m-card-user__pic">
															<img src="{{asset('templante/metronic/default/assets/app/media/img/users/user4.jpg')}}" class="m--img-rounded m--marginless" alt=""/> 
															</div>-->
															<div class="m-card-user__details">
																<span class="m-card-user__name m--font-weight-500">
																	{{ auth()->user()->name }}
																</span>
																<a href="" class="m-card-user__email m--font-weight-300 m-link">
																	{{ auth()->user()->email }}
																</a>
															</div>
														</div>
													</div>
													<div class="m-dropdown__body">
														<div class="m-dropdown__content">
															<ul class="m-nav m-nav--skin-light">
																<li class="m-nav__section m--hide">
																	<span class="m-nav__section-text">
																		Section
																	</span>
																</li>
																<li class="m-nav__item">
																	<a href="/logout" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
																		Cerrar Sesión
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
										
									</ul>
								</div>
							</div>
							<!-- END: Topbar -->
						</div>
					</div>
				</div>
			</header>
			<!-- END: Header -->		
		<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<!-- BEGIN: Left Aside -->
			
				<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
					<!-- BEGIN: Aside Menu -->
					<div 
						id="m_ver_menu" 
						class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " 
						data-menu-vertical="true"
						data-menu-scrollable="false" data-menu-dropdown-timeout="500"  
						>
		
						<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
							@if(auth()->user()->rol_id === 2 || auth()->user()->rol_id === 3)
								<li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
									
									<a  href="{{ url('home') }}" class="m-menu__link ">
										<i class="m-menu__link-icon flaticon-line-graph"></i>
										<span class="m-menu__link-title">
											<span class="m-menu__link-wrap">
												<span class="m-menu__link-text">
													Turista
												</span>
												
											</span>
										</span>
									</a>
								</li>
							@endif
							
							@if( auth()->user()->rol_id === 1)
								<li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
									
									<a  href="{{ url('dashboard/1') }}" class="m-menu__link ">
										<i class="m-menu__link-icon flaticon-line-graph"></i>
										<span class="m-menu__link-title">
											<span class="m-menu__link-wrap">
												<span class="m-menu__link-text">
													Panel de Control
												</span>
												
											</span>
										</span>
									</a>
								</li>
							@endif
							<li class="m-menu__section">
								<h4 class="m-menu__section-text">
									Gestion
								</h4>
								<i class="m-menu__section-icon flaticon-more-v3"></i>
							</li>

	<!--clases faltantes
	
	m-menu__item--active
	m-menu__item--open m-menu__item--expanded

		  				
-->	
							@if(auth()->user()->rol_id === 2 || auth()->user()->rol_id === 3)
								@if( auth()->user()->profile->name === "Admin")
										@if ($enlace == 'user')
											<li class="m-menu__item m-menu__item--active" aria-haspopup="true"  data-menu-submenu-toggle="hover">
										@else
											<li class="m-menu__item " aria-haspopup="true"  data-menu-submenu-toggle="hover">
										@endif

											<a  href="{{ url('User') }}" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-icon flaticon-profile-1"></i>
												<span class="m-menu__link-text">
													USUARIOS
												</span>
											</a>
								
										</li>
									@endif

									@if( auth()->user()->profile->name === "Admin" || auth()->user()->profile->name === "Edit")
										@if ($enlace == 'client')
											<li class="m-menu__item m-menu__item--active" aria-haspopup="true"  data-menu-submenu-toggle="hover">
										@else
											<li class="m-menu__item " aria-haspopup="true"  data-menu-submenu-toggle="hover">
										@endif

											<a  href="{{ url('Client') }}" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-icon flaticon-user "></i>
												<span class="m-menu__link-text">
													CLIENTES
												</span>
											</a>
								
										</li>
									@endif		

									@if ($enlace == 'item')
									<li class="m-menu__item m-menu__item--active" aria-haspopup="true"  data-menu-submenu-toggle="hover">
									@else
									<li class="m-menu__item " aria-haspopup="true"  data-menu-submenu-toggle="hover">
									@endif

										<a  href="{{ url('Item') }}" class="m-menu__link m-menu__toggle">
											<i class="m-menu__link-icon flaticon-signs"></i>
											<span class="m-menu__link-text">
												ITEMS
											</span>
										</a>
							
									</li>

									<!-- @if ($enlace == 'outstanding')
									<li class="m-menu__item m-menu__item--active" aria-haspopup="true"  data-menu-submenu-toggle="hover">
									@else
									<li class="m-menu__item " aria-haspopup="true"  data-menu-submenu-toggle="hover">
									@endif

										<a  href="{{ url('Outstanding') }}" class="m-menu__link m-menu__toggle">
											<i class="m-menu__link-icon flaticon-light"></i>
											<span class="m-menu__link-text">
												Destacados
											</span>
										</a>
							
									</li> -->

								@if ( auth()->user()->email == 'superuser@gmail.com')
									@if ($enlace == 'setting')
									<li class="m-menu__item m-menu__item--active" aria-haspopup="true"  data-menu-submenu-toggle="hover">
									@else
									<li class="m-menu__item " aria-haspopup="true"  data-menu-submenu-toggle="hover">
									@endif

										<a  href="{{ url('Setting') }}" class="m-menu__link m-menu__toggle">
											<i class="m-menu__link-icon flaticon-cogwheel-2"></i>
											<span class="m-menu__link-text">
												Configuración
											</span>
										</a>
							
									</li>
								@endif
							@endif
                            
						</ul>
					</div>
					<!-- END: Aside Menu -->
				</div>
				<!-- END: Left Aside -->
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<div class="m-content">
		  				@yield('content_admin')
					</div>
				</div>
			</div>
			<!-- end:: Body -->
<!-- begin::Footer -->
			<footer class="m-grid__item		m-footer ">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								2020 &copy; iruTv te conecta
							</span>
						</div>
						
					</div>
				</div>
			</footer>
			<!-- end::Footer -->
		</div>
		<!-- end:: Page -->
    		        <!-- begin::Quick Sidebar -->
			    
	    <!-- begin::Scroll Top -->
		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
			<i class="la la-arrow-up"></i>
		</div>
		<!-- end::Scroll Top -->		    <!-- begin::Quick Nav cambio para probar el deploy -->
		
		<!-- begin::Quick Nav -->	
    	<!--begin::Base Scripts -->
		
		<script type="text/javascript" src="{{asset('plugin/recordimg/scripts/jquery.min.js')}}"></script>
		
		<script src="{{asset('templante/metronic/default/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
		
		<script src="{{asset('templante/metronic/default/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
		<!--end::Base Scripts -->   
        <!--begin::Page Vendors -->
		<!--<script src="{{asset('templante/metronic/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>-->
		<!--end::Page Vendors -->  
		<!--begin::Page Snippets -->
		@yield('scriptSelect2')
		<script src="{{asset('templante/metronic/default/assets/app/js/dashboard.js')}}" type="text/javascript"></script>
		<script src="{{ asset('templante/metronic/default/assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>
		<!--end::Page Snippets -->
		
		@yield('script')
		@yield('scriptmaps')
		@yield('script-dropzone')

		
	</body>
	<!-- end::Body -->
</html>
