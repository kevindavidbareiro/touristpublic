<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_calls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('subcategory_id')->unsigned();
            $table->timestamps();

            $table->foreign("item_id")
                ->references("id")->on("items")
                ->onDelete("cascade");

            $table->foreign("category_id")
                ->references("id")->on("categories")
                ->onDelete("cascade");    

            $table->foreign("subcategory_id")
                ->references("id")->on("sub_categories")
                ->onDelete("cascade");    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_calls');
    }
}
