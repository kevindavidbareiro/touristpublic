<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultimediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multimedia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type',45);
            $table->string('name',250);
            $table->tinyInteger('main')->default(0);
            $table->tinyInteger('imgProfile')->default(0);
            $table->integer('item_id')->unsigned();
            $table->timestamps();

            $table->foreign("item_id")
                  ->references("id")->on("items")
                  ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multimedia');
    }
}
