<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->string('address',250);
            $table->double('latitude');
            $table->double('longitude');
            $table->text('descrption')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->timestamps();


            $table->foreign("user_id")
                  ->references("id")->on("users")
                  ->onDelete("cascade");

            $table->foreign("category_id")
                  ->references("id")->on("categories")
                  ->onDelete("cascade");      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
