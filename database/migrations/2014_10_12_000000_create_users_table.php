<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',45)->nullable();
            $table->string('lastname',45)->nullable();
            $table->string('email',100)->unique();
            $table->string('password',100);
            $table->tinyInteger('active')->default(0);
            $table->integer('profile_id')->unsigned();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign("profile_id")
                  ->references("id")->on("profiles")
                  ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
