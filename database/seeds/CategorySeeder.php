<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name'          => 'Pasear',
        ]);
        DB::table('categories')->insert([
            'name'          => 'Comer',
        ]);
        DB::table('categories')->insert([
            'name'          => 'Dormir',
        ]);
        DB::table('categories')->insert([
            'name'          => 'Compras & Servicios',
        ]);
        DB::table('categories')->insert([
            'name'          => 'Divertirse',
        ]);
        DB::table('categories')->insert([
            'name'          => 'Combustible',
        ]);
        DB::table('categories')->insert([
            'name'          => 'Dinero',
        ]);
        DB::table('categories')->insert([
            'name'          => 'Info. Útil',
        ]);
        DB::table('categories')->insert([
            'name'          => 'Conocer',
        ]);
    }
} 