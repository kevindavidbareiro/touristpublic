<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'          => 'SuperUser',
            'lastname'      => 'Sistema',
            'email'         => 'superuser@gmail.com',
            'password'      => bcrypt('123'),
            'active'        => 1,
            'profile_id'    => 1,
        ]);
    }
}
