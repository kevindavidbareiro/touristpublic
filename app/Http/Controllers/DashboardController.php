<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InfoCall;
use App\InfoItems;
use Carbon\Carbon;
use App\Category;
use App\SubCategory;
use App\Item;
use Barryvdh\DomPDF\Facade as PDF;

class DashboardController extends Controller
{
    public function index(){
        
        $listCategory            = $this->getSearcheOfCategory();
        $listSubcategory         = $this->getSearcheOfSubCategory();
        $listItems               = $this->getSearcheOfItem();
        $listCallCategories      = $this->getCallOfCategory();
        $listCallSubCategories   = $this->getCallOfSubCategory();
        $listCallItems           = $this->getCallOfItem();
        $listSuscription         = $this->getSearchesOfSuscription();
        $listCallSuscription     = $this->getCallOfSuscription();

        

        return view('home.home')
            ->with('listCategory',$listCategory)
            ->with('listSubcategory',$listSubcategory)
            ->with('listItems',$listItems)
            ->with('listCallCategories',$listCallCategories)
            ->with('listCallSubCategories',$listCallSubCategories)
            ->with('listCallItems',$listCallItems)
            ->with('listSuscription',$listSuscription)
            ->with('listCallSuscription',$listCallSuscription);
    
    }
    // public function showReport(Request $request){
        
    //     if ($request->category == 'call') {
    //         return view('home.callByMonth');
    //     } else {
        
    //         $list = $this->getSearcheOfItems();
    //         return view('home.peopleByCategory')
    //             ->with('list',$list);
    //     }
        
    // }
    
    public function getCall($msg){
        $countCallByMonths = [0,0,0,0,0,0,0,0,0,0,0,0];
        $callByMonths      = [];
        $totalCalls        = 0;
        $calls             = InfoCall::orderBy('created_at', 'asc')->get();
        foreach ($calls as $key => $call) {
            $callCarbon         = Carbon::parse($call->created_at);
            $month              = $callCarbon->month;
            $callByMonths[$key] = $month;
        }
        $values = array_count_values($callByMonths);
        foreach ($values as $key => $value) {
            $totalCalls                =  $totalCalls + $value;
            $countCallByMonths[$key-1] = $value;
        }
       
        if ($msg == 'dashboart') {
            return response()->json([
                'months' => $countCallByMonths,
                'total'  => $totalCalls
    
            ]);
        } 

        if ($msg == 'report') {
            $data = Carbon::now();
            $pdf  = PDF::loadView('home.report.RcallByMonths', compact('countCallByMonths','totalCalls','data'));
            return $pdf->stream();
        } 
           
    }
    

    public function getSearcheOfCategory(){
        $infoBycategories = [];
        $idcategories     = [];
        $count            = 0;
        $information      = InfoItems::get();
        $categories       = Category::get();
        
        foreach ($information as $key => $info) {
            $idcategories[$key] = $info->category_id;
        }
        $values = array_count_values($idcategories);
        arsort($values);
        
        foreach ($values as $idItem => $val) {
            foreach ($categories as $key => $category) {
                $id = $category->id;
                if ($id == $idItem) {
                    $infoBycategories[$count] = [$id,$category->name,$val];
                    $count++;
                }
                
            }
        }
        
        return $infoBycategories;
        
        
    }

    public function getSearcheOfCategoryByMonth()
    {
        $lisOctober       = [];
        $listNovember     = [];
        $listDecember     = [];
        $infoBycategories = [];
        $listMonth        = [];
        $information      = InfoItems::orderBy('created_at', 'asc')->with(['category'])->get();

        foreach ($information as $info) {
            $date = new Carbon($info->created_at);
            if ($date->month == 10) {
                $lisOctober[] = $info->category->name;
            }
            if ($date->month == 11) {
                $listNovember[] = $info->category->name;
            }
            if ($date->month == 12) {
                $listDecember[] = $info->category->name;
            }
            
        }
       
        $cantByOctober  = array_count_values($lisOctober);
        $cantByNovember = array_count_values($listNovember);
        $cantByDecember = array_count_values($listDecember);

        arsort($cantByOctober);
        arsort($cantByNovember);
        arsort($cantByDecember);

        if (!empty($cantByOctober)) {
            $listMonth['Octubre']=$cantByOctober; 
        }
        if (!empty($cantByNovember)) {
            $listMonth['Noviembre']=$cantByNovember; 
        }

        if (!empty($cantByDecember)) {
            $listMonth['Diciembre']=$cantByDecember; 
        }
        
        return $listMonth;

    }

    public function getSearcheOfSubCategoryByMonth()
    {
        $lisOctober       = [];
        $listNovember     = [];
        $listDecember     = [];
        $infoBycategories = [];
        $listMonth        = [];
        $information      = InfoItems::orderBy('created_at', 'asc')->with(['subcategory'])->get();

        foreach ($information as $info) {
            $date = new Carbon($info->created_at);
            if ($date->month == 10) {
                $lisOctober[] = $info->subcategory->name;
            }
            if ($date->month == 11) {
                $listNovember[] = $info->subcategory->name;
            }
            if ($date->month == 12) {
                $listDecember[] = $info->subcategory->name;
            }
            
        }
       
        $cantByOctober  = array_count_values($lisOctober);
        $cantByNovember = array_count_values($listNovember);
        $cantByDecember = array_count_values($listDecember);

        arsort($cantByOctober);
        arsort($cantByNovember);
        arsort($cantByDecember);

        if (!empty($cantByOctober)) {
            $listMonth['Octubre']=$cantByOctober; 
        }
        if (!empty($cantByNovember)) {
            $listMonth['Noviembre']=$cantByNovember; 
        }

        if (!empty($cantByDecember)) {
            $listMonth['Diciembre']=$cantByDecember; 
        }
        
        return $listMonth;

    }

    public function getSearcheOfItemByMonth()
    {
        $lisOctober       = [];
        $listNovember     = [];
        $listDecember     = [];
        $infoBycategories = [];
        $listMonth        = [];
        $information      = InfoItems::orderBy('created_at', 'asc')->with(['item'])->get();

        foreach ($information as $info) {
            $date = new Carbon($info->created_at);
            if ($date->month == 10) {
                $lisOctober[] = $info->item->name;
            }
            if ($date->month == 11) {
                $listNovember[] = $info->item->name;
            }
            if ($date->month == 12) {
                $listDecember[] = $info->item->name;
            }
            
        }
       
        $cantByOctober  = array_count_values($lisOctober);
        $cantByNovember = array_count_values($listNovember);
        $cantByDecember = array_count_values($listDecember);

        arsort($cantByOctober);
        arsort($cantByNovember);
        arsort($cantByDecember);

        if (!empty($cantByOctober)) {
            $listMonth['Octubre']=$cantByOctober; 
        }
        if (!empty($cantByNovember)) {
            $listMonth['Noviembre']=$cantByNovember; 
        }

        if (!empty($cantByDecember)) {
            $listMonth['Diciembre']=$cantByDecember; 
        }
        
        return $listMonth;

    }

    public function getSearcheOfSubscriptionByMonth()
    {
        $lisOctober          = [];
        $listNovember        = [];
        $listDecember        = [];
        $infoBycategories    = [];
        $listMonth           = [];
        $Octoberbasic        = 0;
        $Octobersemipremium  = 0;
        $Octoberpremium      = 0;
        $Novemberbasic       = 0;
        $Novembersemipremium = 0;
        $Novemberpremium     = 0;
        $Decemberbasic       = 0;
        $Decembersemipremium = 0;
        $Decemberpremium     = 0;
        $information      = InfoItems::orderBy('created_at', 'asc')->with(['item'])->get();

        foreach ($information as $info) {
            $date = new Carbon($info->created_at);
            if ($date->month == 10) {
                if ($info->item->subscription == 1) {
                    $Octoberbasic++;
                }
                if ($info->item->subscription == 2) {
                    $Octobersemipremium++;
                }
                if ($info->item->subscription == 3) {
                    $Octoberpremium++;
                }
            }
            if ($date->month == 11) {
                if ($info->item->subscription == 1) {
                    $Novemberbasic++;
                }
                if ($info->item->subscription == 2) {
                    $Novembersemipremium++;
                }
                if ($info->item->subscription == 3) {
                    $Novemberpremium++;
                }
            }
            if ($date->month == 12) {
                if ($info->item->subscription == 1) {
                    $Decemberbasic++;
                }
                if ($info->item->subscription == 2) {
                    $Decembersemipremium++;
                }
                if ($info->item->subscription == 3) {
                    $Decemberpremium++;
                }
            }
            
        }
       
        $cantByOctober  = ['SemiPremium'=>$Octobersemipremium,'Premium'=>$Octoberpremium,'Básico'=>$Octoberbasic];
        $cantByNovember = ['SemiPremium'=>$Novembersemipremium,'Premium'=>$Novemberpremium,'Básico'=>$Novemberbasic];
        // $cantByDecember = ['SemiPremium'=>$Decembersemipremium,'Premium'=>$Decemberpremium,'Básico'=>$Decemberbasic];
        
        arsort($cantByOctober);
        arsort($cantByNovember);
        // arsort($cantByDecember);

        if (!empty($cantByOctober)) {
            $listMonth['Octubre']=$cantByOctober; 
        }
        if (!empty($cantByNovember)) {
            $listMonth['Noviembre']=$cantByNovember; 
        }

        // if (!empty($cantByDecember)) {
        //     $listMonth['Diciembre']=$cantByDecember; 
        // }
        return $listMonth;

    }

    public function getCallOfCategoryByMonth()
    {
        $lisOctober       = [];
        $listNovember     = [];
        $listDecember     = [];
        $infoBycategories = [];
        $listMonth        = [];
        $information      = InfoCall::orderBy('created_at', 'asc')->with(['category'])->get();

        foreach ($information as $info) {
            $date = new Carbon($info->created_at);
            if ($date->month == 10) {
                $lisOctober[] = $info->category->name;
            }
            if ($date->month == 11) {
                $listNovember[] = $info->category->name;
            }
            if ($date->month == 12) {
                $listDecember[] = $info->category->name;
            }
            
        }
       
        $cantByOctober  = array_count_values($lisOctober);
        $cantByNovember = array_count_values($listNovember);
        $cantByDecember = array_count_values($listDecember);

        arsort($cantByOctober);
        arsort($cantByNovember);
        arsort($cantByDecember);

        if (!empty($cantByOctober)) {
            $listMonth['Octubre']=$cantByOctober; 
        }
        if (!empty($cantByNovember)) {
            $listMonth['Noviembre']=$cantByNovember; 
        }

        if (!empty($cantByDecember)) {
            $listMonth['Diciembre']=$cantByDecember; 
        }
        
        return $listMonth;

    }

    public function getCallOfSubCategoryByMonth()
    {
        $lisOctober       = [];
        $listNovember     = [];
        $listDecember     = [];
        $infoBycategories = [];
        $listMonth        = [];
        $information      = InfoCall::orderBy('created_at', 'asc')->with(['subcategory'])->get();

        foreach ($information as $info) {
            $date = new Carbon($info->created_at);
            if ($date->month == 10) {
                $lisOctober[] = $info->subcategory->name;
            }
            if ($date->month == 11) {
                $listNovember[] = $info->subcategory->name;
            }
            if ($date->month == 12) {
                $listDecember[] = $info->subcategory->name;
            }
            
        }
       
        $cantByOctober  = array_count_values($lisOctober);
        $cantByNovember = array_count_values($listNovember);
        $cantByDecember = array_count_values($listDecember);

        arsort($cantByOctober);
        arsort($cantByNovember);
        arsort($cantByDecember);

        if (!empty($cantByOctober)) {
            $listMonth['Octubre']=$cantByOctober; 
        }
        if (!empty($cantByNovember)) {
            $listMonth['Noviembre']=$cantByNovember; 
        }

        if (!empty($cantByDecember)) {
            $listMonth['Diciembre']=$cantByDecember; 
        }
        
        return $listMonth;

    }

    public function getCallOfItemByMonth()
    {
        $lisOctober       = [];
        $listNovember     = [];
        $listDecember     = [];
        $infoBycategories = [];
        $listMonth        = [];
        $information      = InfoCall::orderBy('created_at', 'asc')->with(['item'])->get();

        foreach ($information as $info) {
            $date = new Carbon($info->created_at);
            if ($date->month == 10) {
                $lisOctober[] = $info->item->name;
            }
            if ($date->month == 11) {
                $listNovember[] = $info->item->name;
            }
            if ($date->month == 12) {
                $listDecember[] = $info->item->name;
            }
            
        }
       
        $cantByOctober  = array_count_values($lisOctober);
        $cantByNovember = array_count_values($listNovember);
        $cantByDecember = array_count_values($listDecember);

        arsort($cantByOctober);
        arsort($cantByNovember);
        arsort($cantByDecember);

        if (!empty($cantByOctober)) {
            $listMonth['Octubre']=$cantByOctober; 
        }
        if (!empty($cantByNovember)) {
            $listMonth['Noviembre']=$cantByNovember; 
        }

        if (!empty($cantByDecember)) {
            $listMonth['Diciembre']=$cantByDecember; 
        }
        
        return $listMonth;

    }

    public function getCallOfSubscriptionByMonth()
    {
        $lisOctober          = [];
        $listNovember        = [];
        $listDecember        = [];
        $infoBycategories    = [];
        $listMonth           = [];
        $Octoberbasic        = 0;
        $Octobersemipremium  = 0;
        $Octoberpremium      = 0;
        $Novemberbasic       = 0;
        $Novembersemipremium = 0;
        $Novemberpremium     = 0;
        $Decemberbasic       = 0;
        $Decembersemipremium = 0;
        $Decemberpremium     = 0;
        $information      = InfoCall::orderBy('created_at', 'asc')->with(['item'])->get();

        foreach ($information as $info) {
            $date = new Carbon($info->created_at);
            if ($date->month == 10) {
                if ($info->item->subscription == 1) {
                    $Octoberbasic++;
                }
                if ($info->item->subscription == 2) {
                    $Octobersemipremium++;
                }
                if ($info->item->subscription == 3) {
                    $Octoberpremium++;
                }
            }
            if ($date->month == 11) {
                if ($info->item->subscription == 1) {
                    $Novemberbasic++;
                }
                if ($info->item->subscription == 2) {
                    $Novembersemipremium++;
                }
                if ($info->item->subscription == 3) {
                    $Novemberpremium++;
                }
            }
            if ($date->month == 12) {
                if ($info->item->subscription == 1) {
                    $Decemberbasic++;
                }
                if ($info->item->subscription == 2) {
                    $Decembersemipremium++;
                }
                if ($info->item->subscription == 3) {
                    $Decemberpremium++;
                }
            }
            
        }
       
        $cantByOctober  = ['SemiPremium'=>$Octobersemipremium,'Premium'=>$Octoberpremium,'Básico'=>$Octoberbasic];
        $cantByNovember = ['SemiPremium'=>$Novembersemipremium,'Premium'=>$Novemberpremium,'Básico'=>$Novemberbasic];
        // $cantByDecember = ['SemiPremium'=>$Decembersemipremium,'Premium'=>$Decemberpremium,'Básico'=>$Decemberbasic];
        
        arsort($cantByOctober);
        arsort($cantByNovember);
        // arsort($cantByDecember);

        if (!empty($cantByOctober)) {
            $listMonth['Octubre']=$cantByOctober; 
        }
        if (!empty($cantByNovember)) {
            $listMonth['Noviembre']=$cantByNovember; 
        }

        // if (!empty($cantByDecember)) {
        //     $listMonth['Diciembre']=$cantByDecember; 
        // }
        return $listMonth;

    }

    public function getSearcheOfSubCategory(){
        $infoBycategories = [];
        $idcategories     = [];
        $count            = 0;
        $information      = InfoItems::get();
        $subCategories    = SubCategory::with(['category'])->get();
        foreach ($information as $key => $info) {
            $idcategories[$key] = $info->subcategory_id;
        }
        $values = array_count_values($idcategories);
        arsort($values);

        foreach ($values as $idItem => $val) {
            foreach ($subCategories as $key => $subCategory) {
                $id = $subCategory->id;
                if ($id == $idItem) {
                    $infoBySubcategories[$count] = [$id,$subCategory->name,$subCategory->category->name,$val];
                    $count++;
                }
                
            }
        }

        return $infoBySubcategories;
        
    }


    public function getSearcheOfItem(){
        $infoByItems = [];
        $iditem      = [];
        $count       = 0;
        $information = InfoItems::get();
        $items       = Item::with(['category'])->get();
        foreach ($information as $key => $info) {
            $iditem[$key] = $info->item_id;
        }
        $values = array_count_values($iditem);
        arsort($values);

        foreach ($values as $idItem => $val) {
            foreach ($items as $key => $item) {
                $id = $item->id;
                if ($id == $idItem) {
                    $infoByItems[$count] = [$id,$item->name,$item->category->name,$val];
                    $count++;
                }
                
            }
        }
    
        return $infoByItems;
        
        
    }

    public function getSearchesOfSuscription(){
        $basic       = 0;
        $semipremium = 0;
        $premium     = 0;
        $information = InfoItems::with(['item'])->get();
        $listSub     = [];
        
        foreach ($information as $info) {
            if ($info->item->subscription == 1) {
                $basic++;
            }
            if ($info->item->subscription == 2) {
                $semipremium++;
            }
            if ($info->item->subscription == 3) {
                $premium++;
            }
        }
        $listSub = ['SemiPremium'=>$semipremium,'Premium'=>$premium,'Básico'=>$basic];
        arsort($listSub);

        return $listSub;
        
    }

    public function getCallOfSuscription(){
        $basic       = 0;
        $semipremium = 0;
        $premium     = 0;
        $information = InfoCall::with(['item'])->get();
        $listSub     = [];
        
        foreach ($information as $info) {
            if ($info->item->subscription == 1) {
                $basic++;
            }
            if ($info->item->subscription == 2) {
                $semipremium++;
            }
            if ($info->item->subscription == 3) {
                $premium++;
            }
        }
        $listSub = ['SemiPremium'=>$semipremium,'Premium'=>$premium,'Básico'=>$basic];
        arsort($listSub);

        return $listSub;
        
    }

    public function getCallOfItem(){
        $infoByItems = [];
        $iditem      = [];
        $count       = 0;
        $information = InfoCall::get();
        $items       = Item::with(['category'])->get();
        foreach ($information as $key => $info) {
            $iditem[$key] = $info->item_id;
        }
        $values = array_count_values($iditem);
        arsort($values);

        foreach ($values as $idItem => $val) {
            foreach ($items as $key => $item) {
                $id = $item->id;
                if ($id == $idItem) {
                    $infoByItems[$count] = [$id,$item->name,$item->category->name,$val];
                    $count++;
                }
                
            }
        }
    
        return $infoByItems;
        
        
    }

    public function getCallOfCategory(){
        $callByCategory = [];
        $iditem         = [];
        $count          = 0;
        $information    = InfoCall::get();
        $categories     = Category::get();
        foreach ($information as $key => $info) {
            $iditem[$key] = $info->category_id;
        }
        $values = array_count_values($iditem);
        arsort($values);

        foreach ($values as $idItem => $val) {
            foreach ($categories as $key => $category) {
                $id = $category->id;
                if ($id == $idItem) {
                    $callByCategory[$count] = [$id,$category->name,$val];
                    $count++;
                }
                
            }
        }
    
        return $callByCategory;
        
        
    }

    public function getCallOfSubCategory(){
        $callBySubCategory = [];
        $iditem            = [];
        $count             = 0;
        $information       = InfoCall::get();
        $subcategories     = SubCategory::with(['category'])->get();
        foreach ($information as $key => $info) {
            $iditem[$key] = $info->subcategory_id;
        }
        $values = array_count_values($iditem);
        arsort($values);


        foreach ($values as $idItem => $val) {
            foreach ($subcategories as $key => $subcategory) {
                $id = $subcategory->id;
                if ($id == $idItem) {
                    $callBySubCategory[$count] = [$id,$subcategory->name,$subcategory->category->name,$val];
                    $count++;
                }
            }
        }

        return $callBySubCategory;
        
    }

    public function RsearchesByCategory()
    {
        $listCategory  = $this->getSearcheOfCategory();
        $data          = Carbon::now();

        // dd($date->format('d/m/Y'),$date->format('H:i'));

        $pdf = PDF::loadView('home.report.RsearchesByCategory', compact('listCategory','data'));
        return $pdf->stream();
        
    }

    public function RsearchesBySubCategory()
    {
        $listSubCategory = $this->getSearcheOfSubCategory();
        $data            = Carbon::now();

        $pdf = PDF::loadView('home.report.RsearchesBySubCategory', compact('listSubCategory','data'));
        return $pdf->stream();
        
    }

    public function RsearchesByItem()
    {
        $listItems = $this->getSearcheOfItem();
        $data      = Carbon::now();

        $pdf = PDF::loadView('home.report.RsearchesByItem', compact('listItems','data'));
        return $pdf->stream();
        
    }

    public function RcallByCategory()
    {
        $listCallCategories  = $this->getCallOfCategory();
        $data = Carbon::now();

        $pdf = PDF::loadView('home.report.RcallByCategory', compact('listCallCategories','data'));
        return $pdf->stream();
        
    }

    public function RcallBySubCategory()
    {
        $listCallSubCategories   = $this->getCallOfSubCategory();
        $data = Carbon::now();

        $pdf = PDF::loadView('home.report.RcallBySubCategory', compact('listCallSubCategories','data'));
        return $pdf->stream();
        
    }

    public function RcallByItem()
    {
        $listCallItems = $this->getCallOfItem();
        $data = Carbon::now();

        $pdf = PDF::loadView('home.report.RcallByItem', compact('listCallItems','data'));
        return $pdf->stream();
        
    }

    public function RsearchesBySuscription()
    {
        $listSubscription = $this->getSearchesOfSuscription();
        $data = Carbon::now();

        $pdf = PDF::loadView('home.report.RsearchesBySubscription', compact('listSubscription','data'));
        return $pdf->stream();
        
    }

    public function RcallBySuscription()
    {
        $listCallSubscription = $this->getCallOfSuscription();
        $data = Carbon::now();

        $pdf = PDF::loadView('home.report.RcallBySuscription', compact('listCallSubscription','data'));
        return $pdf->stream();
        
    }

    public function RsearchesOfCategoryByMonth()
    {
        $listCategoryByMonth  = $this->getSearcheOfCategoryByMonth();
        $data = Carbon::now();
        $pdf = PDF::loadView('home.report.RsearchesOfCategoryByMonth', compact('listCategoryByMonth','data'));
        return $pdf->stream();
    }

    public function RsearchesOfSubCategoryByMonth()
    {
        $listSubCategoryByMonth  = $this->getSearcheOfSubCategoryByMonth();
        $data = Carbon::now();
        $pdf = PDF::loadView('home.report.RsearchesOfSubCategoryByMonth', compact('listSubCategoryByMonth','data'));
        return $pdf->stream();
    }

    public function RsearchesOfItemByMonth()
    {
        $listItemByMonth  = $this->getSearcheOfItemByMonth();
        $data = Carbon::now();
        $pdf = PDF::loadView('home.report.RsearchesOfItemByMonth', compact('listItemByMonth','data'));
        return $pdf->stream();
    }

    public function RsearchesOfSubscriptionByMonth()
    {
        $listSubscriptionByMonth  = $this->getSearcheOfSubscriptionByMonth();
        $data = Carbon::now();
        $pdf = PDF::loadView('home.report.RsearchesOfSubscriptionByMonth', compact('listSubscriptionByMonth','data'));
        return $pdf->stream();
    }


    public function RcallsOfCategoryByMonth()
    {
        $listCategoryByMonth  = $this->getCallOfCategoryByMonth();
        $data = Carbon::now();
        $pdf = PDF::loadView('home.report.RcallsOfCategoryByMonth', compact('listCategoryByMonth','data'));
        return $pdf->stream();
    }

    public function RcallsOfSubCategoryByMonth()
    {
        $listSubCategoryByMonth  = $this->getCallOfSubCategoryByMonth();
        $data = Carbon::now();
        $pdf = PDF::loadView('home.report.RcallsOfSubCategoryByMonth', compact('listSubCategoryByMonth','data'));
        return $pdf->stream();
    }

    public function RcallsOfItemByMonth()
    {
        $listItemByMonth  = $this->getCallOfItemByMonth();
        $data = Carbon::now();
        $pdf = PDF::loadView('home.report.RcallsOfItemByMonth', compact('listItemByMonth','data'));
        return $pdf->stream();
    }

    public function RcallsOfSubscriptionByMonth()
    {
        $listSubscriptionByMonth  = $this->getCallOfSubscriptionByMonth();
        $data = Carbon::now();
        $pdf = PDF::loadView('home.report.RcallsOfSubscriptionByMonth', compact('listSubscriptionByMonth','data'));
        return $pdf->stream();
    }

    
}
