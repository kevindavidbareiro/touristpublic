<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Item;

class DeliveryController extends Controller
{
    public function getDeliverys(){
        return Item::where('subCategory', 23)->with(['multimedia'])->get();
    }
}
