<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Category;
use App\SubCategory;
use App\Item;



class ItemController extends Controller
{    
    public function index(){
        $listItem = Item::with(['category'])->orderBy('id', 'desc')->get();
        return view('Item.list')
                ->with('listItem', $listItem);
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $categories = Category::all();
        $SubCategory = SubCategory::all();
        

        
        
        return view('Item.create')
                ->with('categories', $categories)
                ->with('SubCategory', $SubCategory);
    }

    public function show($id)
    {
        $listItem = Item::with(['category'])->orderBy('id', 'desc')->get();
        return view('Item.list')
                ->with('listItem', $listItem);
  
       
    }
   
    public function store(Request $request){
        
        $data = request()->validate([
            'name'         => 'required',
            'address'      => 'required',
            'category'     => 'required',
            'latitude'     => 'required',
            'longitude'    => 'required',
            'description'  => 'required',
        ], [
            'name.required'         => 'name',
            'address.required'      => 'address',
            'category.required'     => 'category',
            'latitude.required'     => 'latitude',
            'longitude.required'    => 'longitude',
            'description.required'  => 'description',
        ]);

       
        $iduser = auth()->user()->id;
        $address = $this->AddressShort($request->address);
        
        $item = new Item;

        $item->name          = $request->name;
        $item->address       = $address;
        $item->phone         = $request->phone;
        $item->latitude      = $request->latitude;
        $item->longitude     = $request->longitude;
        $item->facebook      = $request->facebook;
        $item->instagram     = $request->instagram;
        $item->twitter       = $request->twitter;
        $item->web           = $request->web;
        $item->descrption    = $request->description;
        $item->descriptionEn = $request->descriptionEn;
        $item->category_id   = $request->category;
        $item->subscription  = $request->subscription;
        $item->keywords      = $request->keywords;
        if ($request->category == 7) {$item->netAtms  = $request->netAtms;}
        $item->star          = $request->nstar;
        $item->SubCategory   = $request->SubCategory;
        $item->user_id       = $iduser;
        if($item->save())
        {
            return redirect('Item')
                    ->with('typemsg', 'success')
                    ->with('message', 'Item creado correctamente.');
        }
        else {
            return redirect('Item')
                    ->with('typemsg', 'error')
                    ->with('message', 'Upps hubo un problema al guardar el Item.');
        }
    }
    
    public function edit($id)
    {
        $item = Item::with(['category'])->findOrFail($id);

        
        return view('Item.update')
                    ->with('item', $item)
                    ->with('categories',Category::all())
                    ->with('SubCategory',SubCategory::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $data = request()->validate([
            'name'         => 'required',
            'address'      => 'required',
            'category'     => 'required',
            'latitude'     => 'required',
            'longitude'    => 'required',
            'description'  => 'required',
        ], [
            'name.required'         => 'name',
            'address.required'      => 'address',
            'category.required'     => 'category',
            'latitude.required'     => 'latitude',
            'longitude.required'    => 'longitude',
            'description.required'  => 'description',
        ]);

        $iduser = auth()->user()->id;
        $address = $this->AddressShort($request->address);
        
        $item = Item::findOrFail($id);
        $item->name          = $request->name;
        $item->address       = $address;
        $item->phone         = $request->phone;
        $item->latitude      = $request->latitude;
        $item->longitude     = $request->longitude;
        $item->category_id   = $request->category;
        $item->facebook      = $request->facebook;
        $item->instagram     = $request->instagram;
        $item->twitter       = $request->twitter;
        $item->web           = $request->web;
        $item->descrption    = $request->description;
        $item->descriptionEn = $request->descriptionEn;
        $item->subscription  = $request->subscription;
        $item->keywords      = $request->keywords;
        if ($request->category == 7){
            $item->netAtms  = $request->netAtms;
        }else{
            $item->netAtms  = null;
        }
        $item->star          = $request->nstar;
        $item->SubCategory   = $request->SubCategory;
        $item->user_id       = $iduser;
    
        if($item->save())
        {
            return redirect('Item')
                    ->with('typemsg', 'success')
                    ->with('message', 'El Item se a modificado correctamente.');
        }
        else {
            return redirect('Item')
                    ->with('typemsg', 'error')
                    ->with('message', 'Upps hubo un problema al modificar el Item.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::find($id);

        if($item->delete())
        {
            return redirect('Item')
                ->with('typemsg', 'success')
                ->with('message', 'Item borrado correctamente.');
        }
        else {
            return redirect('Item')
                ->with('typemsg', 'error')
                ->with('message', 'Upps al parecer hubo un error al borrar el item.');
        }
    }

    //FOR API REST
    public function getItemsForCategory($idcategory)
    {
        return Item::with(["multimedia", "category"])
                ->where("category_id", $idcategory)
                ->orderBy("subscription", 'DESC')
                ->inRandomOrder()
                ->get();
    }

    public function getItemsForSubCategory($idsubcategory)
    {
        return Item::with(["multimedia", "category"])
            ->where("subCategory", $idsubcategory)
            ->orderBy("subscription", 'DESC')
            ->inRandomOrder()
            ->get();
    }
    
    public function getQueryItems($query)
    {
        return Item::with(["multimedia", "category"])
            ->where("name", "like", "%{$query}%")
            ->orWhere("descrption", "like", "%{$query}%")
            ->orWhere("keywords", "like", "%{$query}%")
            ->get();
    }

    public function getNewsItemsForSubCategory($idsubcategory)
    {
        return Item::with(["multimedia", "category"])
            ->where("subCategory", $idsubcategory)
            ->orderBy("subscription", 'DESC')
            ->get();
    }

    public function getDetailsOfItem($id)
    {
        return Item::with(["multimedia", "category"])->where("id", $id)->get();
    }

    public function AddressShort($address){
        $arrayAddress = explode(",", $address);

        return $arrayAddress[0];
    }

    public function getOutstandingOfItem($id)
    {
        $nulls           = [];
        $orders          = [];
        $list            = [];
        $listOutstanding = [];
        $conN            = 0;
        $conO            = 0;
        $conL            = 0;
        
        $outstanding = Item::where("category_id","=",$id)
        ->where("outstanding","=",1)
        ->with(["multimedia", "category"])
        ->orderby('order_of_outstanding','asc')
        ->get();

        
        if (count($outstanding)) {
            
            foreach ($outstanding as $item) {
                if ($item->order_of_outstanding == null) {
                    $nulls[$conN] = [$item];
                    $conN++;
                } else {
                    $orders[$conO] = [$item];
                    $conO++;
                }
            }
    
            foreach ($orders as $order) {
                $list[$conL] = [$order];
                $conL++;
            }
    
            foreach ($nulls as $null) {
                $list[$conL] = [$null];
                $conL++;
            }
    
            foreach ($list as $key => $l) {
                $listOutstanding[$key] = $l[0][0];
    
            }

        } else {
            
            $listOutstanding = [];
        }
        
        return $listOutstanding;
    }
}
