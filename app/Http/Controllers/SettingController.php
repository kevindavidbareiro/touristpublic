<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;

class SettingController extends Controller
{
    public function index(){
        $listSubCategory = Subcategory::with(['category'])->orderBy('id', 'desc')->get();
        return view('Setting.subcategory.list')
                ->with('listSubCategory', $listSubCategory);
        
    } 


    public function store(Request $request){
        
    }
}
