<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;


class CategoryController extends Controller
{
    public function getCategories(){
        $finaCategories = [];
        $categories = Category::all();
        foreach ($categories as $cat) {
            $cat->selected = false;
            array_push($finaCategories, $cat);
        }

     

        return response()->json([
            'categories' => $finaCategories
        ]);
    }
}
