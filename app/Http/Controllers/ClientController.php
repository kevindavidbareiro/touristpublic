<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Rol;
use App\Profile;
use App\Item;
use App\User;
use App\Client;



class ClientController extends Controller
{    
    public function index(){
        $listClient = Client::with(['user','user.profile','item'])->get();
        return view('Client.list')
                ->with('listClient', $listClient);
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profiles = Profile::all();
        $items = Item::all();

        return view('Client.create')
                ->with('profiles', $profiles)
                ->with('items', $items)
                ;
    }
   
    public function store(Request $request){
        
       

        $data = request()->validate([
            'name'       => 'required',
            'lastname'   => 'required',
            'email'      => 'required|unique:users,email',
            'password'   => 'required',
            'item'       => 'required',
        ], [
            'name.required'         => 'name',
            'lastname.required'     => 'lastname',
            'email.required'        => 'email',
            'password.required'     => 'password',
            'item.required'         => 'item',
        ]);

         $user = new User;

         $user->name        = $request->name;
         $user->lastname    = $request->lastname;
         $user->email       = $request->email;
         $user->password    = bcrypt($request->password);
         $user->profile_id  = $request->admin;
         $user->rol_id      = $request->client;
        
         if($user->save())
         {
            $client = new Client;

            $client->user_id  = $user->id;
            $client->item_id  = $request->item;

            if($client->save()){
                return redirect('Client/create')
                      ->with('typemsg', 'success')
                      ->with('message', 'Usuario creado correctamente.');
            }else{
                return redirect('Client/create')
                    ->with('typemsg', 'error')
                    ->with('message', 'Upps hubo un problema al guardar el Usuario.');

            } 
         }
         else {
            return redirect('Client/create')
                    ->with('typemsg', 'error')
                     ->with('message', 'Upps hubo un problema al guardar el Usuario.');
         }
    }
    
    public function edit($id)
    {
        
        $client = Client::with(['user','item'])->findOrFail($id);

        return view('Client.update')
                    ->with('client', $client)
                    ->with('items',Item::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
        if (User::where('email', $request->email)->exists()) {
                
                $verCLient = Client::findOrFail($id);
                $verUser   = User::findOrFail($verCLient->user_id);


                if ($request->email === $verUser->email) {
                    $data = request()->validate([
                        'name'       => 'required',
                        'lastname'   => 'required',
                        'email'      => 'required',
                        'item'       => 'required',
                    ], [
                        'name.required'         => 'name',
                        'lastname.required'     => 'lastname',
                        'email.required'        => 'email',
                        'item.required'         => 'item',
                    ]);
                }else{
                    $data = request()->validate([
                        'name'       => 'required',
                        'lastname'   => 'required',
                        'email'      => 'required|unique:users,email',
                        'item'       => 'required',
                    ], [
                        'name.required'         => 'name',
                        'lastname.required'     => 'lastname',
                        'email.required'        => 'email',
                        'item.required'         => 'item',
                    ]);
                }

        }else{
            $data = request()->validate([
                'name'       => 'required',
                'lastname'   => 'required',
                'email'      => 'required|unique:users,email',
                'item'       => 'required',
            ], [
                'name.required'         => 'name',
                'lastname.required'     => 'lastname',
                'email.required'        => 'email',
                'item.required'         => 'item',
            ]);
        }
        
        
        

        

        $client = Client::findOrFail($id);
        $client->item_id = $request->item;
       

        if($client->save())
        {

            $user = User::findOrFail($client->user_id);
            $user->name        = $request->name;
            $user->lastname    = $request->lastname;
            $user->email       = $request->email;
            if ($request->password != null) {
                $user->password       = $request->password;
            }

            if($user->save()){
                return redirect('Client')
                     ->with('typemsg', 'success')
                     ->with('message', 'El Cliente se a modificado correctamente.');
                 
            }else {
             return redirect('Client')
                    ->with('typemsg', 'error')
                     ->with('message', 'Upps hubo un problema al modificar al Cliente.');
            }
            
         }
         else {
             return redirect('Client')
                    ->with('typemsg', 'error')
                     ->with('message', 'Upps hubo un problema al modificar al Cliente.');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $client = Client::findOrFail($id);
        $idUserClient = $client->user_id;

        if($client->delete())
        {
             $user = User::findOrFail($idUserClient);
            if($user->delete()){
                return redirect('Client')
                ->with('typemsg', 'success')
                 ->with('message', 'Cliente borrado correctamente.');
            } else {
                return redirect('Client')
                    ->with('typemsg', 'error')
                    ->with('message', 'Upps al parecer hubo un error al borrar el cliente.');
            }
            
         }
        else {
            return redirect('Client')
                 ->with('typemsg', 'error')
                 ->with('message', 'Upps al parecer hubo un error al borrar el cliente.');
         }
    }

    // public function showpassword($id){
    
    //     $user = User::findOrFail($id);
    //     return view('User.password')
    //             ->with('user', $user);
    // }

    // public function password(Request $request,$id){
    
        
    //     $data = request()->validate([
    //         'password'          => 'required',
           
    //     ], [
    //         'password.required' => 'password',
    //     ]);

    //     $user = User::findOrFail($id);
    //     $user->password   = bcrypt($request->password);
    //     if($user->save())
    //     {
    //         return redirect('User')
    //                 ->with('typemsg', 'success')
    //                 ->with('message', 'La contraseña se a modificado correctamente.');
    //     }
    //     else {
    //         return redirect('User')
    //                 ->with('typemsg', 'error')
    //                 ->with('message', 'Upps hubo un problema al modificar la contraseña.');
    //     }
    // }
}
