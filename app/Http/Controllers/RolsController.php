<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use App\Rol;


class RolsController extends Controller
{
    public function index(){
        
        $listUser = User::with(['profile'])->get();
        $rols     = Rol::all();

        return view('superUser.rols')
            ->with('listUser', $listUser)
            ->with('rols', $rols);
        
    } 

    public function edit($id)
    {
        $user = User::with(['rols'])->findOrFail($id);

        return view('superUser.updateRols')
                    ->with('user', $user)
                    ->with('rols',Rol::all());
    }



    public function update(Request $request, $id){
       
        $user = User::findOrFail($id);
        $user->rol_id   = $request->rol;
     

        if($user->save())
        {
            return redirect('updateRolsdeUsuarios')
                    ->with('typemsg', 'success')
                    ->with('message', 'El Usuario se a modificado correctamente.');
        }
        else {
            return redirect('updateRolsdeUsuarios')
                    ->with('typemsg', 'error')
                    ->with('message', 'Upps hubo un problema al modificar al Usuario.');
        }
    }


}
