<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Client;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }
    
    public function authenticate(Request $request)
    {
        
        if (Auth::attempt(['email' => $request->emial, 'password' => $request->password])) {
            
            if (auth()->user()->rol_id === 2 || auth()->user()->rol_id === 3) {
                return redirect('/home');
            }

            if (auth()->user()->rol_id === 1) {
               
                return redirect('/dashboard');
            }
           
        }
        else {
            return redirect('/login')
                ->with('error','Usuario o password incorrectos.');
        }
    }

    public function logout()
    {
        Auth::logout();
        return view('auth.login');
    }
}
