<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use App\Item;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        
        
        return view('Setting.subcategory.create')
            ->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = request()->validate([
            'name'         => 'required',
            'category'     => 'required',
            'icon'         => 'required|image|mimes:png',
            
        ], [
            'name.required'         => 'name',
            'category.required'     => 'category',
            'icon.required'         => 'El ícono es obligatorio.',
            'icon.mimes'            => 'Solo se acepta formato PNG.',
            'icon.image'            => 'Solo se acepta imágenes.'
        ]);

        $imageName = time().'.'.request()->icon->getClientOriginalExtension();

        $subCategory = new SubCategory;

        $subCategory->name          = $request->name;
        $subCategory->category_id   = $request->category;
        $subCategory->icon          = $imageName;
      
        if($subCategory->save()) 
        {
            request()->icon->move(public_path('img/icons'), $imageName);

            return redirect('Setting')
                    ->with('typemsg', 'success')
                    ->with('message', 'Categoría creado correctamente.');
        }
        else 
        {
            return redirect('Setting')
                    ->with('typemsg', 'error')
                    ->with('message', 'Upps hubo un problema al guardar el Categoría.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $SubCategory = Subcategory::with(['category'])->findOrFail($id);

        
        return view('Setting.subcategory.update')
                    ->with('SubCategory', $SubCategory)
                    ->with('categories',Category::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $data = request()->validate([
            'name'         => 'required',
            'icon'         => 'image|mimes:png',
        ], [
            'name.required'         => 'name',
            'icon.mimes'            => 'Solo se acepta formato PNG.',
            'icon.image'            => 'Solo se acepta imágenes.'
        ]);

        
        $SubCategory = SubCategory::findOrFail($id);
        $SubCategory->name          = $request->name;
        if(request()->icon){
            $imageName = time().'.'.request()->icon->getClientOriginalExtension();
            $SubCategory->icon      = $imageName;
        }
      
       
        if($SubCategory->save())
        {
            if(request()->icon){
                request()->icon->move(public_path('img/icons'), $imageName);
            }
            return redirect('Setting')
                    ->with('typemsg', 'success')
                    ->with('message', 'La categoría se a modificado correctamente.');
        }
        else {
            return redirect('Setting')
                    ->with('typemsg', 'error')
                    ->with('message', 'Upps hubo un problema al modificar la categoría.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $SubCategory = SubCategory::find($id);
        $listByCategory = Item::where('subCategory', $id)->get();
        foreach ($listByCategory as $subCategory) {
            $Item = Item::findOrFail($subCategory->id);
            $Item->subCategory    = null;
            $Item->save();
        }

        if($SubCategory->delete())
        {
            return redirect('Setting')
                ->with('typemsg', 'success')
                ->with('message', 'Categoría borradada correctamente.');
        }
        else {
            return redirect('Setting')
                ->with('typemsg', 'error')
                ->with('message', 'Upps al parecer hubo un error al borrar la Categoría.');
        }
    }


    public function getSubCategoryByCategory($id)
    {
        $SubCategory = SubCategory::where('category_id', '=', $id)->get();

        return response()->json([
            'SubCategory' => $SubCategory
        ]);

    }
    
    //PARA LA API, DEVUELVE LAS SUBCATEGORÍAS DE UNA CATEGORÍA
    public function getApiSubCategoriesByCategory($id)
    {
        return $subCategories = SubCategory::where('category_id', '=', $id)->get();
    }

}
