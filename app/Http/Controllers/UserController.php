<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Profile;
use App\User;


class UserController extends Controller
{    
    public function index(){
        $listUser = User::with(['profile'])->get()->where('rol_id','!=', 1);


        
        return view('User.list')
                ->with('listUser', $listUser);
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profiles = Profile::all();

        return view('User.create')
                ->with('profiles', $profiles);
    }
   
    public function store(Request $request){
        
        $data = request()->validate([
            'name'       => 'required',
            'lastname'   => 'required',
            'email'      => 'required|unique:users,email',
            'password'   => 'required',
            'profile'    => 'required',
        ], [
            'name.required'         => 'name',
            'lastname.required'     => 'lastname',
            'email.required'        => 'email',
            'password.required'     => 'password',
            'profile.required'      => 'profile',
        ]);

        $user = new User;

        $user->name        = $request->name;
        $user->lastname    = $request->lastname;
        $user->email       = $request->email;
        $user->password    = bcrypt($request->password);
        $user->profile_id  = $request->profile;
        $user->rol_id      = $request->sistem;
        
        if($user->save())
        {
            return redirect('User/create')
                    ->with('typemsg', 'success')
                    ->with('message', 'Usuario creado correctamente.');
        }
        else {
            return redirect('User/create')
                    ->with('typemsg', 'error')
                    ->with('message', 'Upps hubo un problema al guardar el Usuario.');
        }
    }
    
    public function edit($id)
    {
        $user = User::with(['profile'])->findOrFail($id);

        return view('User.update')
                    ->with('user', $user)
                    ->with('profiles',Profile::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        
        $data = request()->validate([
            'name'       => 'required',
            'lastname'   => 'required',
            'email'      => 'required|unique:users,email',
            'profile'    => 'required',
        ], [
            'name.required'         => 'name',
            'lastname.required'     => 'lastname',
            'email.required'        => 'email',
            'profile.required'      => 'profile',
        ]);

        $user = User::findOrFail($id);
        $user->name        = $request->name;
        $user->lastname    = $request->lastname;
        $user->email       = $request->email;
        $user->profile_id  = $request->profile;

        if($user->save())
        {
            return redirect('User')
                    ->with('typemsg', 'success')
                    ->with('message', 'El Usuario se a modificado correctamente.');
        }
        else {
            return redirect('User')
                    ->with('typemsg', 'error')
                    ->with('message', 'Upps hubo un problema al modificar al Usuario.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if($user->delete())
        {
            return redirect('User')
                ->with('typemsg', 'success')
                ->with('message', 'Usuario borrado correctamente.');
        }
        else {
            return redirect('list')
                ->with('typemsg', 'error')
                ->with('message', 'Upps al parecer hubo un error al borrar el usuario.');
        }
    }

    public function showpassword($id){
    
        $user = User::findOrFail($id);
        return view('User.password')
                ->with('user', $user);
    }

    public function password(Request $request,$id){
    
        
        $data = request()->validate([
            'password'          => 'required',
           
        ], [
            'password.required' => 'password',
        ]);

        $user = User::findOrFail($id);
        $user->password   = bcrypt($request->password);
        if($user->save())
        {
            return redirect('User')
                    ->with('typemsg', 'success')
                    ->with('message', 'La contraseña se a modificado correctamente.');
        }
        else {
            return redirect('User')
                    ->with('typemsg', 'error')
                    ->with('message', 'Upps hubo un problema al modificar la contraseña.');
        }
    }
}
