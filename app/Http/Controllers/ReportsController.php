<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\InfoCall;
use App\InfoItems;

class ReportsController extends Controller
{
    public function infoCall(Request $request)
    {
        try {
            $info = new InfoCall;
    
            $info->item_id          = $request->item_id;
            $info->category_id      = $request->category_id;
            $info->subcategory_id   = $request->subcategory_id;

            $info->save();
            
            return response()->json([
                'status' => 'ok'
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'error'  => $e->getMessage()
            ]);
        }
    }

    public function infoItem(Request $request)
    {
        try {
            $info = new InfoItems;

            $info->item_id          = $request->item_id;
            $info->category_id      = $request->category_id;
            $info->subcategory_id   = $request->subcategory_id;

            $info->save();

            return response()->json([
                'status' => 'ok'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'error'  => $e->getMessage()
            ]);
        }
    }
}
