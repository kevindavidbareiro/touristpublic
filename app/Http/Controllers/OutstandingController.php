<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use App\Item;

class OutstandingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $listItem = Item::with(['category'])->orderBy('id', 'desc')->get();
        $categories = Category::all();
        
        return view('outstanding.list')
                ->with('categories', $categories);
    }

    public function getItemByCategory($id)
    {
        $ItemByCategory = Item::where('category_id', '=', $id)->get();

        return response()->json([
            'ItemByCategory' => $ItemByCategory,
        ]);

    }

    public function getOutstandingByCategory($id)
    {
        $nulls  = [];
        $orders = [];
        $list   = [];
        $conN   = 0;
        $conO   = 0;
        $conL   = 0;
        
        $outstanding = Item::where("category_id","=",$id)
        ->where("outstanding","=",1)
        ->orderby('order_of_outstanding','asc')
        ->get();


        foreach ($outstanding as $item) {
            if ($item->order_of_outstanding == null) {
                $nulls[$conN] = [$item];
                $conN++;
            } else {
                $orders[$conO] = [$item];
                $conO++;
            }
        }

        foreach ($orders as $order) {
            $list[$conL] = [$order];
            $conL++;
        }

        foreach ($nulls as $null) {
            $list[$conL] = [$null];
            $conL++;
        }


        return response()->json([
            'outstanding' => $list,
        ]);
    }

    public function getOutstanding($idCategory,$valCheck)
    {
        $ItemByCategory = Item::where('category_id', '=', $idCategory)->get();

        return response()->json([
            'datos' => $ItemByCategory,
        ]);
    }

    public function storeOutstanding($checkbox,$idCategory)
    {   
        $ItemByCategory = Item::where('category_id', '=', $idCategory)->get();
        foreach ($ItemByCategory as $items) {
            $item = Item::findOrFail($items->id);
            $item->outstanding = 0;
            $item->save();
        }

        if ($checkbox != 0) {
            $Item2 = Item::where('category_id', '=', $idCategory)->get();
            $checks = explode(',',$checkbox);

            foreach ($checks as $key => $check) {
                foreach ($Item2 as $items2) {
                    if ($items2->id == intval($checks[$key])) {
                        $item = Item::findOrFail($items2->id);
                        $item->outstanding = 1;
                        $item->save();
                    } 
                }
            }
        }

        $Items = Item::where('category_id', '=', $idCategory)->get();

        foreach ($Items as $item) {
            if($item->outstanding != 1){
                $item->order_of_outstanding = null;
                $item->save();
            }
        }
            
        return response()->json([
            'data' => 'ok',
        ]);
        
    }

    public function storeRanking($idItem,$newRanking)
    {   

        $item = Item::findOrFail($idItem);
        $item->order_of_outstanding = $newRanking;
        $item->save();
        

        return response()->json([
            'data' => 'ok',
        ]);
        
    }
}
