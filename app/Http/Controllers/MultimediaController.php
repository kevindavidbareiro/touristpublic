<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

use App\Http\Helpers\Helpers;
use App\Multimedia;
use App\Item;
use Image;


class MultimediaController extends Controller
{    
    const MESSAGE_SUCCESS = 1;
    const MESSAGE_ERROR   = 2;

    public function index(){
       
    } 


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $itemMultimedia =  Item::with('multimedia')->find($id);
        $images = [];

        foreach ($itemMultimedia['multimedia'] as $img) {
           
            $images[] = [$img->id,$img->name];
            
        }

        //busco la imagen principal anterior para cambiar el campo main
        $main = Multimedia::where("item_id","=",$id)
                                    ->where("main","=",1)
                                    ->get();
        
        return view('multimedia.create')
                    ->with('idItem', $id)
                    ->with('images', $images)
                    ->with('main', $main);
    }
    public function storeMain(Request $request ,$id , $name){
        
        $type         = explode(".", $name);
        //obtengo la ruta de la imagen original
        $urlImgstart = public_path() . '/img/'.$name;

        if ($type[1] == 'gif') {
            //obtengo la ruta de la imagen final
            $urlImgEnd   = public_path() . '/img/main/'.$name;
            copy($urlImgstart,$urlImgEnd);
        } else {
            //obtengo la ruta de la imagen final
            $urlImgEnd   = public_path() . '/img/main/';
            //creo una nueva imagen
            $imgMain     = Image::make($urlImgstart);
            //la redimenciono
            $imgMain->fit(1024, 500);
            //guardo imagen en archivo
            $imgMain->save($urlImgEnd . $name);
        }
        

        //busco la nueva imagen principal para cambiar el campo main
        $multimediaMain = Multimedia::find($id);
        $ItemId = $multimediaMain->item_id;

        //busco la imagen principal anterior para cambiar el campo main
        $PreviousMain = Multimedia::where("item_id","=",$ItemId)
                                    ->where("main","=",1)
                                    ->get();

        if (count($PreviousMain) != 0) {
            $PreviousMain[0]->main   = 0;
            $multimediaMain->main    = 1;

            $name      = $PreviousMain[0]->name;
            $images    = File::files(public_path() . '/img/main');
            $deleteImg = array_filter($images, function ($var) use ($name) {
                return preg_match("/\b$name\b/i", $var);
            });

            if ($PreviousMain[0]->save() && $multimediaMain->save() && File::delete($deleteImg)) {
                return 'ok';
            }
        }

        $multimediaMain->main    = 1;
        if ($multimediaMain->save()){
            return 'ok';
        }
        
    }
    
   
    public function store(Request $request){
        
        $idItem   = $request->idItem;
        $path_img = public_path() . '/img/';
       


        foreach ($request->file('file') as $file) {
            
            $originalName   = $file->getClientOriginalName();
            $type           = explode(".", $originalName);
            $name           = Helpers::generateRandomString();
            $Cant           = count($type);
            $imgName        = $idItem .'-'.$name.'.'.$type[$Cant-1];
            $file->move($path_img,$imgName);
            //Image::make($file)->save($path_img . $imgName);


            $Multimedia = new Multimedia;

            $Multimedia->type    = $type[1];
            $Multimedia->name    = $imgName;
            $Multimedia->item_id = $idItem;
            $Multimedia->save();
            
            
        }

        return redirect('Multimedia')
            ->with('message_type', self::MESSAGE_SUCCESS)
            ->with('message', 'LA IMAGEN SE AGREGÓ CORRECTAMENTE.');
    }
    
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $multimedia = Multimedia::find($id);
        $name      = $multimedia->name;
        $images    = File::files(public_path() . '/img/');
        $deleteImg = array_filter($images, function ($var) use ($name) {
            return preg_match("/\b$name\b/i", $var);
        });

        if ($multimedia->delete() and File::delete($deleteImg)) {
            return 'ok';
        }
        return 'no';
        
    }


    public function storeGif(Request $request){
        
        $idItem   = $request->idItem;
        $path_img = public_path() . '/img/';
        $file = $request->file('file');
        $originalName   = $file->getClientOriginalName();
        $type           = explode(".", $originalName);
        $name           = Helpers::generateRandomString();
        $Cant           = count($type);
        $imgName        = $idItem .'-'.$name.'.'.$type[$Cant-1];
        
        $file->move($path_img,$imgName);
        return "ok";

    }

}
