<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\InfoItems;
use App\InfoCall;
use App\Client;
use Carbon\Carbon;



class DashboardItemController extends Controller
{
    public function index(){
        $dateToday = Carbon::now();

    
        $Client = Client::where('user_id','=',auth()->user()->id)->with(['item'])->first();

        $id   = $Client->item_id;
        $item = $Client->item->name;

       
        $SearcheOfItem        = $this->getSearcheOfItem($id);
        $SearcheOfMonth       = $this->getSearcheOfMonth($id);
        

        $CallOfItem           = $this->getCallOfItem($id);
        $CallOfMonth          = $this->getCallOfMonth($id);

        $month                = $this->getMonth();
      
        return view('Client.dashboard')
            ->with('month',$month)
            ->with('item',$item)
            ->with('id',$id)
            ->with('SearcheOfItem',$SearcheOfItem)
            ->with('SearcheOfMonth',$SearcheOfMonth)
            ->with('CallOfItem',$CallOfItem)
            ->with('CallOfMonth',$CallOfMonth);
    
    }


    public function getMonth(){
        
        $date = Carbon::now();
        
        $month = $date->month;
        
        if ($month === 1)  {$nameMonth = 'Enero';}
        if ($month === 2)  {$nameMonth = 'Febrero';}
        if ($month === 3)  {$nameMonth = 'Marzo';}
        if ($month === 4)  {$nameMonth = 'Abril';}
        if ($month === 5)  {$nameMonth = 'Mayo';}
        if ($month === 6)  {$nameMonth = 'Junio';}
        if ($month === 7)  {$nameMonth = 'Julio';}
        if ($month === 8)  {$nameMonth = 'Agosto';}
        if ($month === 9)  {$nameMonth = 'Septiembre';}
        if ($month === 10) {$nameMonth = 'Octubre';}
        if ($month === 11) {$nameMonth = 'Noviembre';}
        if ($month === 12) {$nameMonth = 'Diciembre';}

        return $nameMonth;
    }

    public function getSearcheOfItem($id){
        
        $SearcheOfItem = InfoItems::where('item_id','=',$id)->count();
    
        return $SearcheOfItem;
   
    }

    public function getSearcheOfMonth($id)
    {
        $date =  Carbon::now();
        $SearcheOfItemMonth = InfoItems::whereYear('created_at', '=', $date->format('Y'))->where('item_id','=',$id)->whereMonth('created_at', '=', $date->month)->count();
        
        return $SearcheOfItemMonth;
    }

    public function getCallOfItem($id){
        
        $CallOfItem = InfoCall::where('item_id','=',$id)->count();
    
        return $CallOfItem;
   
    }

    public function getCallOfMonth($id)
    {
        $date =  Carbon::now();
        $CallOfItemMonth = InfoCall::where('item_id','=',$id)->whereMonth('created_at', '=', $date->month)->count();
        
        return $CallOfItemMonth;
    }

    public function SearcheOfItem7days($id){
        
        $dateToday = Carbon::now();
   
        
        $date7 = $dateToday->format('Y-m-d');
        $date6 = $dateToday->subDays(1)->format('Y-m-d');
        $date5 = $dateToday->subDays(1)->format('Y-m-d');
        $date4 = $dateToday->subDays(1)->format('Y-m-d');
        $date3 = $dateToday->subDays(1)->format('Y-m-d');
        $date2 = $dateToday->subDays(1)->format('Y-m-d');
        $date1 = $dateToday->subDays(1)->format('Y-m-d');
      
        
        $SearcheItemDay7 = InfoItems::whereDate('created_at','=',$date7)->where('item_id','=',$id)->count();
        $SearcheItemDay6 = InfoItems::whereDate('created_at','=',$date6)->where('item_id','=',$id)->count();
        $SearcheItemDay5 = InfoItems::whereDate('created_at','=',$date5)->where('item_id','=',$id)->count();
        $SearcheItemDay4 = InfoItems::whereDate('created_at','=',$date4)->where('item_id','=',$id)->count();
        $SearcheItemDay3 = InfoItems::whereDate('created_at','=',$date3)->where('item_id','=',$id)->count();
        $SearcheItemDay2 = InfoItems::whereDate('created_at','=',$date2)->where('item_id','=',$id)->count();
        $SearcheItemDay1 = InfoItems::whereDate('created_at','=',$date1)->where('item_id','=',$id)->count(); 
        
    
       $week = array(
           $date1, 
           $date2,
           $date3,
           $date4,
           $date5,
           $date6,
           $date7,
        );

        $end7Days = array(
           $SearcheItemDay1,
           $SearcheItemDay2,
           $SearcheItemDay3,
           $SearcheItemDay4,
           $SearcheItemDay5,
           $SearcheItemDay6,
           $SearcheItemDay7

        );

       
        return response()->json([
            'days' => $end7Days,
            'week' => $week,
        ]);
        
   
    }

    public function CallOfItem7days($id){
        
        $dateToday = Carbon::now();
        
        $date7 = $dateToday->format('Y-m-d');
        $date6 = $dateToday->subDays(1)->format('Y-m-d');
        $date5 = $dateToday->subDays(1)->format('Y-m-d');
        $date4 = $dateToday->subDays(1)->format('Y-m-d');
        $date3 = $dateToday->subDays(1)->format('Y-m-d');
        $date2 = $dateToday->subDays(1)->format('Y-m-d');
        $date1 = $dateToday->subDays(1)->format('Y-m-d');
      
        $CallItemDay7 = InfoCall::whereDate('created_at','=',$date7)->where('item_id','=',$id)->count();
        $CallItemDay6 = InfoCall::whereDate('created_at','=',$date6)->where('item_id','=',$id)->count();
        $CallItemDay5 = InfoCall::whereDate('created_at','=',$date5)->where('item_id','=',$id)->count();
        $CallItemDay4 = InfoCall::whereDate('created_at','=',$date4)->where('item_id','=',$id)->count();
        $CallItemDay3 = InfoCall::whereDate('created_at','=',$date3)->where('item_id','=',$id)->count();
        $CallItemDay2 = InfoCall::whereDate('created_at','=',$date2)->where('item_id','=',$id)->count();
        $CallItemDay1 = InfoCall::whereDate('created_at','=',$date1)->where('item_id','=',$id)->count(); 
        
       $week = array(
           $date1, 
           $date2,
           $date3,
           $date4,
           $date5,
           $date6,
           $date7,
        );

        $end7Days = array(
           $CallItemDay1,
           $CallItemDay2,
           $CallItemDay3,
           $CallItemDay4,
           $CallItemDay5,
           $CallItemDay6,
           $CallItemDay7

        );

       
        return response()->json([
            'Calldays' => $end7Days,
            'Callweek' => $week,
        ]);
        
   
    }

    public function getSearcheForMonths($id){
        
        $item7day= [];
        $date = Carbon::now();
      
        $January   = InfoItems::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 1)->where('item_id','=',$id)->count();
        $Februay   = InfoItems::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 2)->where('item_id','=',$id)->count();
        $March     = InfoItems::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 3)->where('item_id','=',$id)->count();
        $April     = InfoItems::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 4)->where('item_id','=',$id)->count();
        $May       = InfoItems::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 5)->where('item_id','=',$id)->count();
        $June      = InfoItems::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 6)->where('item_id','=',$id)->count(); 
        $July      = InfoItems::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 7)->where('item_id','=',$id)->count();
        $August    = InfoItems::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 8)->where('item_id','=',$id)->count();
        $September = InfoItems::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 9)->where('item_id','=',$id)->count();
        $October   = InfoItems::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 10)->where('item_id','=',$id)->count();
        $November  = InfoItems::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 11)->where('item_id','=',$id)->count();
        $December  = InfoItems::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 12)->where('item_id','=',$id)->count();
        
       $MonthForYears = array(
           $January,
           $Februay,
           $March,
           $April,
           $May,
           $June,
           $July,
           $August,
           $September,
           $October,
           $November,
           $December

        );
  
        return response()->json([
            'MonthForYears' => $MonthForYears,
        ]);
        
   
    }

    public function getCallForMonths($id){
        
        
        $date = Carbon::now();
      
        $January   = InfoCall::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 1)->where('item_id','=',$id)->count();
        $Februay   = InfoCall::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 2)->where('item_id','=',$id)->count();
        $March     = InfoCall::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 3)->where('item_id','=',$id)->count();
        $April     = InfoCall::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 4)->where('item_id','=',$id)->count();
        $May       = InfoCall::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 5)->where('item_id','=',$id)->count();
        $June      = InfoCall::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 6)->where('item_id','=',$id)->count(); 
        $July      = InfoCall::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 7)->where('item_id','=',$id)->count();
        $August    = InfoCall::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 8)->where('item_id','=',$id)->count();
        $September = InfoCall::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 9)->where('item_id','=',$id)->count();
        $October   = InfoCall::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 10)->where('item_id','=',$id)->count();
        $November  = InfoCall::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 11)->where('item_id','=',$id)->count();
        $December  = InfoCall::whereYear('created_at', '=', $date->format('Y'))->whereMonth('created_at', '=', 12)->where('item_id','=',$id)->count();
        
       $MonthForYearsCall = array(
           $January,
           $Februay,
           $March,
           $April,
           $May,
           $June,
           $July,
           $August,
           $September,
           $October,
           $November,
           $December

        );
  
        return response()->json([
            'MonthForYearsCall' => $MonthForYearsCall,
        ]);
        
   
    }
}
