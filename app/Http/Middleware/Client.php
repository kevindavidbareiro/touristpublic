<?php

namespace App\Http\Middleware;

use Closure;


class Client
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->rol_id != 1) {
            return redirect('login');
        }

        return $next($request);
    }
}
