<?php

namespace App\Http\Helpers;

class Helpers
{

    public static function enlaceActivo($url)
    {
		//quita los numeros en los enlaces
		$urlsinN = preg_replace('/[0-9]+/', '', $url);
		//$urls = explode('_', $url);
	
        $urls = [
            ['/', '/'],
            ['home', 'home'],
            ['report', 'home'],
            ['User', 'user'],
            ['User/create', 'user'],
            ['User//edit', 'user'],
            ['User//changePassword', 'user'],
            ['Client', 'client'],
            ['Client/create', 'client'],
            ['Client//edit', 'client'],
            ['Item', 'item'],
            ['Item/create', 'item'],
            ['Item/', 'item'],
            ['Item//edit', 'item'],
            ['Multimedia', 'item'],
            ['Multimedia/', 'item'],
            ['Main', 'item'],
            ['pruebados', 'item'],
            ['Setting', 'setting'],
            ['SubCategory/create', 'setting'],
            ['SubCategory//edit', 'setting'],
            ['Outstanding', 'outstanding'],
            ['dashboard', 'dashboardClient'],

            ['updateRolsdeUsuarios', 'superuser'],
            ['updateRolsdeUsuarios/', 'superuser'],
        ];

        foreach ($urls as list($a, $b)) {
            // $a contiene el primer elemento del array interior,
            // y $b contiene el segundo elemento.
            if ($urlsinN == $a) {
                $enlace = $b;
                break;
            }
        }
       
        return $enlace;
    }
    
    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
	
   
    public static function TextConerter($text)
    {
        $textmod = ucwords(strtolower($text));
        return $textmod;
    }


    public static function arrayErrors($errors){
        dd($errors,'en helpers');
    }

}
