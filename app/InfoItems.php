<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoItems extends Model
{
    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\SubCategory');
    }
}
