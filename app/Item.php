<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function multimedia()
    {
        return $this->hasMany('App\Multimedia')->orderBy('created_at', 'desc');
    }

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function locations()
    {
        return $this->belongsTo('App\Location');
    }

    public function clients()
    {
        return $this->hasMany('App\Client');
    }
}
