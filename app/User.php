<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }

    public function rols()
    {
        return $this->belongsTo('App\Rol');
    }

    public function item()
    {
        return $this->hasMany('App\Item');
    }

    public function clients()
    {
        return $this->hasMany('App\Client');
    }

    
}
