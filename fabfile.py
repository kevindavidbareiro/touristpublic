from __future__ import with_statement
from time import time

from fabric.api import cd, run, env
from fabric.decorators import task
from fabric.contrib.files import exists

env.use_ssh_config = True


releases_dir = "/home/root/tourist/releases"
git_branch = "master"
env.hosts = ['tourist']
git_repo = "git@bitbucket.org:KevinBareiro/tourist.git"
repo_dir = "/home/root/tourist/repo"
persist_dir = "/home/root/tourist/persist"
next_release = "%(time).0f" % {'time': time()}
current_release = "/home/root/tourist/current"


@task
def deploy(migrate='no'):
    init()
    update_git()
    create_release()
    build_site()

    if migrate=='yes':
        migrate_from = "%s/%s" % (releases_dir, next_release)
        migrate_forward(migrate_from)

    swap_symlinks()

@task
def migrate():
    migrate_forward()

@task
def migrate_back():
    migrate_backward()


def set_hosts():
    env.hosts = ['host1', 'host2']
    
def migrate_forward(release_dir=None, env='production'):
    if not release_dir:
        release_dir=current_release
    with cd(release_dir):
        run('php artisan migrate --env=%s' % env)

def migrate_backward(release_dir=None, env='production'):
    if not release_dir:
        release_dir = current_release
    with cd(release_dir):
        run('php artisan migrate:rollback --env=%s' % env)

def init():
    if not exists(releases_dir):
        run("mkdir -p %s" % releases_dir)

    if not exists(repo_dir):
        run("git clone -b %s %s %s" % (git_branch, git_repo, repo_dir) )

    if not exists("%s/storage" % persist_dir):
        run("mkdir -p %s/storage/app" % persist_dir)
        run("mkdir -p %s/storage/framework/cache" % persist_dir)
        run("mkdir -p %s/storage/framework/sessions" % persist_dir)
        run("mkdir -p %s/storage/framework/views" % persist_dir)
        run("mkdir -p %s/storage/logs" % persist_dir)

    if not exists("%s/img" % persist_dir):
        run("mkdir -p %s/img" % persist_dir)
        run("mkdir -p %s/img/main" % persist_dir)    

    '''
    if not exists("%s/uploads" % persist_dir):
        run("mkdir -p %s/uploads" % persist_dir)
        run("mkdir -p %s/uploads/thumbs" % persist_dir)

    if not exists("%s/banners" % persist_dir):
        run("mkdir -p %s/banners" % persist_dir)

    if not exists("%s/imgsecciones" % persist_dir):
        run("mkdir -p %s/imgsecciones" % persist_dir)    
        run("mkdir -p %s/imgsecciones/images" % persist_dir)    
        run("mkdir -p %s/imgsecciones/imgtelesalud" % persist_dir)    
    '''
def update_git():
    with cd(repo_dir):
        run("git checkout %s" % git_branch)
        run("git pull origin %s" % git_branch)

def create_release():
    release_into = "%s/%s" % (releases_dir, next_release)
    run("mkdir -p %s" % release_into)
    with cd(repo_dir):
        run("git archive --worktree-attributes %s | tar -x -C %s" % (git_branch, release_into))

def build_site():
    with cd("%s/%s" % (releases_dir, next_release)):
        run("rm composer.lock")
        run("composer install")

def swap_symlinks():
    release_into = "%s/%s" % (releases_dir, next_release)

    run("ln -nfs %s/.env %s/.env" % (persist_dir, release_into))
    run("rm -rf %s/storage" % release_into)
    #run("rm -rf %s/public/uploads" % release_into)
    #run("rm -rf %s/public/assets/frontend/images/banners" % release_into)
    #run("rm -rf %s/public/assets/frontend/images/imgsecciones" % release_into)

    run("ln -nfs %s/storage %s/storage" % (persist_dir, release_into))
    #run("ln -nfs %s/uploads %s/public" % (persist_dir, release_into))
    run("ln -nfs %s/img %s/public/img" % (persist_dir, release_into))
    #run("ln -nfs %s/img/main %s/public/img/main" % (persist_dir, release_into))

    run("ln -nfs %s %s" % (release_into, current_release))

    run("sudo service php7.2-fpm reload")
